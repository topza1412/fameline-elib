<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('page-offline', function () {
    return view('page_offline');
});

Route::group( [ 'middleware' => 'status_web'], function()
{
	//route user
	Route::get('/', 'Member\HomeController@index');

	Route::get('login', 'Member\LoginController@index');
	Route::post('login/auth', 'Member\LoginController@auth');
	Route::get('logout', 'Member\LoginController@logout');

	Route::get('register', 'Member\RegisterController@register');

	Route::get('forgotpassword', 'Member\ForgotPasswordController@index');
	Route::post('forgotpassword/sendmail', 'Member\ForgotPasswordController@sendmail');
	Route::get('forgotpassword/changepassword/{token_email}',
	'Member\ForgotPasswordController@changepassword');
	Route::post('forgotpassword/verify', 'Member\ForgotPasswordController@verify');

	//dashboard after login
	Route::group( [ 'middleware' => 'session_login_user'], function(){

		Route::get('dashboard', 'Member\DashboardController@index');
		Route::get('changepassword', 'Member\ChangePasswordController@index');
		Route::post('changepassword/verify', 'Member\ChangePasswordController@verify');
		Route::get('category/{level}/{id}', 'Member\CategoriesController@main');
		Route::post('knowledge/comment', 'Member\KnowledgeController@comment');
		Route::get('knowledge/favorite/{id?}', 'Member\KnowledgeController@favorite');

	});
	
	Route::post('knowledge/search', 'Member\KnowledgeController@search');
	Route::get('knowledge/detail/{id?}', 'Member\KnowledgeController@detail');
	Route::get('knowledge/like/{id?}', 'Member\KnowledgeController@like');
	
	Route::get('contact', 'Member\ContactController@index');
	//end route user

});




//route admin
Route::get('admin/login', 'Admin\LoginController@index');
Route::post('admin/login/auth', 'Admin\LoginController@auth');
Route::get('admin/logout', 'Admin\LoginController@logout');

//forgotpassword
Route::get('admin/forgotpassword', 'Admin\ForgotPasswordController@index');
Route::post('admin/forgotpassword/sendmail', 'Admin\ForgotPasswordController@sendmail');
Route::get('admin/forgotpassword/changepassword/{token_email}',
'Admin\ForgotPasswordController@changepassword');
Route::post('admin/forgotpassword/verify', 'Admin\ForgotPasswordController@verify');

Route::group( [ 'middleware' => 'session_login_admin'], function(){

	//home
	Route::get('admin/home', 'Admin\HomeController@index');

	//admin profile
	Route::get('admin/profile/{page}', 'Admin\ProfileController@page');
	Route::post('admin/profile/{page}/action', 'Admin\ProfileController@action');

	//admin setting
	Route::get('admin/setting/{page}', 'Admin\SettingController@page');
	Route::get('admin/setting/{page}/add', 'Admin\SettingController@form_add');
	Route::get('admin/setting/{page}/edit/{id}', 'Admin\SettingController@form_edit');
	Route::post('admin/setting/{page}/action', 'Admin\SettingController@action');
	Route::get('admin/setting/delete/{page}/{id}', 'Admin\SettingController@delete');

	//admin menu
	Route::get('admin/menu/{page}', 'Admin\MenuController@page');
	Route::post('admin/menu/{page}/action', 'Admin\MenuController@action');

	//admin users
	Route::get('admin/member/{page}', 'Admin\MemberController@page');
	Route::get('admin/member/{page}/add', 'Admin\MemberController@form_add');
	Route::get('admin/member/{page}/edit/{id}', 'Admin\MemberController@form_edit');
	Route::post('admin/member/{page}/action', 'Admin\MemberController@action');
	Route::get('admin/member/delete/{page}/{id}', 'Admin\MemberController@delete');

	//admin categories
	Route::get('admin/categories/{page}', 'Admin\CategoriesController@page');
	Route::get('admin/categories/{page}/add', 'Admin\CategoriesController@form_add');
	Route::get('admin/categories/{page}/edit/{id}', 'Admin\CategoriesController@form_edit');
	Route::post('admin/categories/{page}/action', 'Admin\CategoriesController@action');
	Route::get('admin/categories/delete/{page}/{id}', 'Admin\CategoriesController@delete');

	//admin categories level1
	Route::get('admin/subcategories1/{page}', 'Admin\SubCategories1Controller@page');
	Route::get('admin/subcategories1/{page}/add', 'Admin\SubCategories1Controller@form_add');
	Route::get('admin/subcategories1/{page}/edit/{id}', 'Admin\SubCategories1Controller@form_edit');
	Route::post('admin/subcategories1/{page}/action', 'Admin\SubCategories1Controller@action');
	Route::get('admin/subcategories1/delete/{page}/{id}', 'Admin\SubCategories1Controller@delete');

	//admin categories level2
	Route::get('admin/subcategories2/{page}', 'Admin\SubCategories2Controller@page');
	Route::get('admin/subcategories2/{page}/add', 'Admin\SubCategories2Controller@form_add');
	Route::get('admin/subcategories2/{page}/edit/{id}', 'Admin\SubCategories2Controller@form_edit');
	Route::post('admin/subcategories2/{page}/action', 'Admin\SubCategories2Controller@action');
	Route::get('admin/subcategories2/delete/{page}/{id}', 'Admin\SubCategories2Controller@delete');

	//admin section
	Route::get('admin/section/{page}', 'Admin\SectionController@page');
	Route::get('admin/section/{page}/add', 'Admin\SectionController@form_add');
	Route::get('admin/section/{page}/edit/{id}', 'Admin\SectionController@form_edit');
	Route::post('admin/section/{page}/action', 'Admin\SectionController@action');
	Route::get('admin/section/delete/{page}/{id}', 'Admin\SectionController@delete');

	//admin knowledge
	Route::get('admin/knowledge/{page}', 'Admin\KnowledgeController@page');
	Route::get('admin/knowledge/{page}/add', 'Admin\KnowledgeController@form_add');
	Route::get('admin/knowledge/{page}/edit/{id}', 'Admin\KnowledgeController@form_edit');
	Route::post('admin/knowledge/{page}/action', 'Admin\KnowledgeController@action');
	Route::get('admin/knowledge/delete/{page}/{id}', 'Admin\KnowledgeController@delete');
	Route::get('admin/knowledge/report/{id}', 'Admin\KnowledgeController@report');

	//admin report
	Route::get('admin/reports/{page}', 'Admin\ReportsController@page');
	Route::post('admin/reports/{page}/search', 'Admin\ReportsController@search');
	Route::post('admin/reports/{page}/export', 'Admin\ReportsController@export');

});
//end route admin


