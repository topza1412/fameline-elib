<!--plugins ทั้งหมด-->

<!-- sweet alert 2 -->
<script src="{{asset('assets/member/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>

<!-- jquery datepicker-->
<link rel="stylesheet" type="text/css" href="{{asset('assets/member/plugins/jquerydatepicker/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/member/plugins/jquerydatepicker/jquery-ui-timepicker-addon.css')}}">
<script src="{{asset('assets/member/plugins/jquerydatepicker/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/member/plugins/jquerydatepicker/jquery-ui-timepicker-addon.js')}}"></script>
<script src="{{asset('assets/member/plugins/jquerydatepicker/jquery-ui-sliderAccess.js')}}"></script>

<script type="text/javascript">

$('.carousel').carousel({
interval: 5000 //changes the speed
})
  
$('.date_start').datepicker({
    inline: true,
    dateFormat: "dd-mm-yy",
    changeFirstDay: false,
    minDate: 0
});

$('.date_end').datepicker({
    inline: true,
    dateFormat: "dd-mm-yy",
    changeFirstDay: false,
    minDate: 0
});

$('.date_other').datepicker({
    inline: true,
    dateFormat: "dd-mm-yy",
    changeFirstDay: false
});

</script>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v7.0'
  });
};

(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
attribution=setup_tool page_id="110811477334269">
</div>

