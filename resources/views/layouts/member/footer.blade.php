<!-- Start: Footer -->
        <footer class="site-footer">
            
            <div class="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-text col-md-6">
                            <p>2020 © {{SettingWeb::SettingWeb()->Set_Title}}</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <ul>
                                <li><a href="{{url('home')}}">Home</a></li>
                                <!-- <li><a href="{{url('news')}}">News</a></li> -->
                                <li><a href="{{url('contact')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End: Footer -->