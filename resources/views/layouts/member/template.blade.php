<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
<meta charset="utf-8">
<meta name="expires" content="never" >
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

@if($data['seo']['title_seo']!=null)
<title>{{SettingWeb::SettingWeb()->Set_Title.' - '.$data['seo']['title_seo']}}</title>
@else
<title>{{SettingWeb::SettingWeb()->Set_Title}}</title>
@endif

<meta name="description" content="{{$data['seo']['description_seo']}}">
<meta name="keywords" content="{{$data['seo']['keywords_seo']}}">

@if($data['seo']['title_seo']!=null)
<meta property="og:title" content="{{SettingWeb::SettingWeb()->Set_Title.' - '.$data['seo']['title_seo']}}"/>
@else
<meta property="og:title" content="{{SettingWeb::SettingWeb()->Set_Title}}"/>
@endif
@if($data['seo']['logo_seo']!=null)
<meta property="og:image" content="{{$data['seo']['logo_seo']}}"/>
@else
<meta property="og:image" content="{{asset('upload/admin/logo_web/'.SettingWeb::SettingWeb()->Set_Logo)}}"/>
@endif
<meta property="og:image:width" content="660" />
<meta property="og:image:height" content="365" />
<meta property="og:description" content="{{$data['seo']['description_seo']}}"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="{{$data['seo']['url_seo']}}" />


<link rel="icon" href="{{asset('upload/member/shortcut_icon/icon.ico')}}" type="image/x-icon" />


 @include('layouts.member.stylesheet')
 @yield('stylesheet')

</head>

<body>

@include('layouts.member.header')
@yield('header')

@yield('detail')

@include('layouts.member.footer')
@yield('footer')

@include('layouts.member.javascripts')
@yield('javascript')

@include('layouts.member.modal')
@yield('modal')



</body>
</html>