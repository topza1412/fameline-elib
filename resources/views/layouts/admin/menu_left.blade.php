<!-- start:Left Menu -->
    <div id="left-menu">
      <div class="sub-left-menu scroll">
        <ul class="nav nav-list">
            <li><div class="left-bg"></div></li>
            <li class="time">
              <h1 class="animated fadeInLeft">{{date('H:i')}}</h1>
              <p class="animated fadeInRight">{{date('Y-m-d')}}</p>
            </li>
            @if(session('session_admin_status') != 1)
              @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_Member)
                <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-user"></span> Members  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/member/user')}}">Users</a></li>
                  </ul>
                </li>
              @endif
              @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_CategoryAll || session('session_admin_role')->Roa_CategoryCore || session('session_admin_role')->Roa_CategoryAdvance || session('session_admin_role')->Roa_CategoryInnovation)
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryCore)
                    <li><a href="{{url('admin/categories/core')}}">Core Knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryAdvance)
                    <li><a href="{{url('admin/categories/advance')}}">Advanced knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryInnovation)
                    <li><a href="{{url('admin/categories/innovation')}}">Innovation knowledge</a></li>
                  @endif
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories1  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryCore)
                    <li><a href="{{url('admin/subcategories1/core')}}">Core Knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryAdvance)
                    <li><a href="{{url('admin/subcategories1/advance')}}">Advanced knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryInnovation)
                    <li><a href="{{url('admin/subcategories1/innovation')}}">Innovation knowledge</a></li>
                  @endif
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories2  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryCore)
                    <li><a href="{{url('admin/subcategories2/core')}}">Core Knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryAdvance)
                    <li><a href="{{url('admin/subcategories2/advance')}}">Advanced knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryInnovation)
                    <li><a href="{{url('admin/subcategories2/innovation')}}">Innovation knowledge</a></li>
                  @endif
                </ul>
              </li>
              @endif
              @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_Section)
              <li class="ripple"><a class="tree-toggle nav-header"><span class="icon-grid"></span> Section  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/section/group')}}"><span class="icon-grid"></span>Section group</a></li>
                </ul>
              </li>
              @endif
              @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore || session('session_admin_role')->Roa_KnowledgeAdvance || session('session_admin_role')->Roa_KnowledgeInnovation)
                <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-book"></span> Knowledge  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore)
                      <li><a href="{{url('admin/knowledge/core')}}">Core Knowledge</a></li>
                    @endif
                    @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeAdvance)
                    <li><a href="{{url('admin/knowledge/advance')}}">Advanced knowledge</a></li>
                    @endif
                    @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeInnovation)
                    <li><a href="{{url('admin/knowledge/innovation')}}">Innovation knowledge</a></li>
                    @endif
                  </ul>
                </li>
              @endif 
              @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogKnowledge || session('session_admin_role')->Roa_ReportsLogSystem || session('session_admin_role')->Roa_ReportsMember || session('session_admin_role')->Roa_ReportsVisit)
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-print"></span> Reports  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                  @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogKnowledge)
                    <li><a href="{{url('admin/reports/logknowledge')}}">Logs knowledge</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogSystem)
                    <li><a href="{{url('admin/reports/logsystem')}}">Logs system</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsMember)
                    <li><a href="{{url('admin/reports/member')}}">Member</a></li>
                  @endif
                  @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsVisit)
                    <li><a href="{{url('admin/reports/visitweb')}}">Visit web</a></li>
                  @endif
                </ul>
              </li>
              @endif
            @else
                <li class="ripple active"><a href="{{url('admin/home')}}"><span class="fa fa-dashboard"></span>Dashboard</a></li>
                <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-clipboard"></span> Setting  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/setting/website')}}">Website</a></li>
                    <!-- <li><a href="{{url('admin/setting/seo')}}">Seo</a></li> -->
                    <li><a href="{{url('admin/setting/slider')}}">Slide</a></li>
                  </ul>
                </li>
                <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-user"></span> Members  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/member/admin')}}">Admin</a></li>
                    <li><a href="{{url('admin/member/user')}}">Users</a></li>
                  </ul>
                </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/categories/core')}}">Core Knowledge</a></li>
                    <li><a href="{{url('admin/categories/advance')}}">Advanced knowledge</a></li>
                    <li><a href="{{url('admin/categories/innovation')}}">Innovation knowledge</a></li>
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories1  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/subcategories1/core')}}">Core Knowledge</a></li>
                    <li><a href="{{url('admin/subcategories1/advance')}}">Advanced knowledge</a></li>
                    <li><a href="{{url('admin/subcategories1/innovation')}}">Innovation knowledge</a></li>
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories2  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/subcategories2/core')}}">Core Knowledge</a></li>
                    <li><a href="{{url('admin/subcategories2/advance')}}">Advanced knowledge</a></li>
                    <li><a href="{{url('admin/subcategories2/innovation')}}">Innovation knowledge</a></li>
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="icon-grid"></span> Section  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/section/group')}}"><span class="icon-grid"></span>Section group</a></li>
                </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-book"></span> Knowledge  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                      <li><a href="{{url('admin/knowledge/core')}}">Core Knowledge</a></li>
                    <li><a href="{{url('admin/knowledge/advance')}}">Advanced knowledge</a></li>
                    <li><a href="{{url('admin/knowledge/innovation')}}">Innovation knowledge</a></li>
                  </ul>
              </li>
              <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-print"></span> Reports  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/reports/logknowledge')}}">Logs knowledge</a></li>
                    <li><a href="{{url('admin/reports/logsystem')}}">Logs system</a></li>
                    <li><a href="{{url('admin/reports/member')}}">Member</a></li>
                    <li><a href="{{url('admin/reports/visitweb')}}">Visit web</a></li>
                </ul>
              </li>
            @endif
          </ul>
        </div>
    </div>
<!-- end: Left Menu -->


      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                @if(session('session_admin_status') != 1)
                  @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_Member)
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-user"></span> Members  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/member/user')}}">Users</a></li>
                      </ul>
                    </li>
                  @endif
                  @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_CategoryAll || session('session_admin_role')->Roa_CategoryCore || session('session_admin_role')->Roa_CategoryAdvance || session('session_admin_role')->Roa_CategoryInnovation)
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryCore)
                          <li><a href="{{url('admin/categories/core')}}">Core Knowledge</a></li>
                        @endif
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryAdvance)
                          <li><a href="{{url('admin/categories/advance')}}">Advanced knowledge</a></li>
                        @endif
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_CategoryInnovation)
                          <li><a href="{{url('admin/categories/innovation')}}">Innovation knowledge</a></li>
                        @endif
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories1  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/subcategories1/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/subcategories1/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/subcategories1/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories2  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/subcategories2/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/subcategories2/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/subcategories2/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                  @endif
                  @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_Section)
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="icon-grid"></span> Section  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                          <li><a href="{{url('admin/section/group')}}"><span class="icon-grid"></span>Section group</a></li>
                      </ul>
                    </li>
                  @endif
                  @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore || session('session_admin_role')->Roa_KnowledgeAdvance || session('session_admin_role')->Roa_KnowledgeInnovation)
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-book"></span> Knowledge  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore)
                          <li><a href="{{url('admin/knowledge/core')}}">Core Knowledge</a></li>
                        @endif
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeAdvance)
                          <li><a href="{{url('admin/knowledge/advance')}}">Advanced knowledge</a></li>
                        @endif
                        @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeInnovation)
                          <li><a href="{{url('admin/knowledge/innovation')}}">Innovation knowledge</a></li>
                        @endif
                      </ul>
                    </li>
                  @endif 
                  @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogKnowledge || session('session_admin_role')->Roa_ReportsLogSystem || session('session_admin_role')->Roa_ReportsMember || session('session_admin_role')->Roa_ReportsVisit)
                  <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-print"></span> Reports  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    <ul class="nav nav-list tree">
                      @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogKnowledge)
                        <li><a href="{{url('admin/reports/logknowledge')}}">Logs knowledge</a></li>
                      @endif
                      @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsLogSystem)
                        <li><a href="{{url('admin/reports/logsystem')}}">Logs system</a></li>
                      @endif
                      @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsMember)
                        <li><a href="{{url('admin/reports/member')}}">Member</a></li>
                      @endif
                      @if(session('session_admin_role')->Roa_ReportsAll || session('session_admin_role')->Roa_ReportsVisit)
                        <li><a href="{{url('admin/reports/visitweb')}}">Visit web</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
              @else
                    <li class="ripple active"><a href="{{url('admin/home')}}"><span class="fa fa-dashboard"></span>Dashboard</a></li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-clipboard"></span> Setting  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/setting/website')}}">Website</a></li>
                        <!-- <li><a href="{{url('admin/setting/seo')}}">Seo</a></li> -->
                        <li><a href="{{url('admin/setting/slider')}}">Slide</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-user"></span> Members  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/member/admin')}}">Admin</a></li>
                        <li><a href="{{url('admin/member/user')}}">Users</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/categories/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/categories/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/categories/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories1  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/subcategories1/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/subcategories1/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/subcategories1/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-group"></span> Sub Categories2  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/subcategories2/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/subcategories2/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/subcategories2/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="icon-grid"></span> Section  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                          <li><a href="{{url('admin/section/group')}}"><span class="icon-grid"></span>Section group</a></li>
                      </ul>
                    </li>
                     <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-book"></span> Knowledge  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                      <ul class="nav nav-list tree">
                        <li><a href="{{url('admin/knowledge/core')}}">Core Knowledge</a></li>
                        <li><a href="{{url('admin/knowledge/advance')}}">Advanced knowledge</a></li>
                        <li><a href="{{url('admin/knowledge/innovation')}}">Innovation knowledge</a></li>
                      </ul>
                    </li>
                  <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-print"></span> Reports  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    <ul class="nav nav-list tree">
                      <li><a href="{{url('admin/reports/logknowledge')}}">Logs knowledge</a></li>
                      <li><a href="{{url('admin/reports/logsystem')}}">Logs system</a></li>
                      <li><a href="{{url('admin/reports/member')}}">Member</a></li>
                      <li><a href="{{url('admin/reports/visitweb')}}">Visit web</a></li>
                    </ul>
                  </li>
              @endif
            </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->