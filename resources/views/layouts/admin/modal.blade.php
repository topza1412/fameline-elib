<!--welcome admin-->
@if (Session::has('login_success'))
<script type="text/javascript" charset="utf-8">
Swal.fire({
  type: 'success',
  title: 'Welcome admin',
  showConfirmButton: false,
  timer: 3000
})
</script>
@endif

<!-- confirm delete -->
<script type="text/javascript" charset="utf-8">
function confirm_delete(url,id){
Swal.fire({
  title: 'Confirm delete data!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Confirm',
  cancel: 'Delete'
}).then((result) => {
  if (result.value) {
    window.location = url+'/'+id;
  }
})
}
</script>


<!-- show video -->
<script type="text/javascript" charset="utf-8">
function show_video(name,url) {

if(url==''){
var url = "<hr><span class=text-danger>ไม่พบวิดีโอ!</span>";
}
else{
var url = "<hr><iframe width=400 src="+url+"></iframe>"; 
}

Swal.fire({
  title: name,
  html: url,
  showConfirmButton: false
})
}
</script>


<!-- select2 category data -->
<script type="text/javascript">    
$(document).ready(function() {
    /*กำหนดให้ class js-data-example-ajax  เรียกใช้งาน Function Select 2*/
    $(".select2_category_data").select2({
      ajax: {
        url: "{{url('api/admin/category/getdata')}}",/* Url ที่ต้องการส่งค่าไปประมวลผลการค้นข้อมูล*/
        dataType: 'json',
        delay: 250,
        data: function (params) {
            
          return {
            q: params.term, // ค่าที่ส่งไป
            page: params.page
          };

        },
        processResults: function (data, params) {
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;

          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1,
      templateResult: formatRepo, // omitted for brevity, see the source of this page
      templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

});


function formatRepo (repo) {

  if (repo.loading) return repo.text;
  
  var markup = "<div class='select2-result-repository clearfix'>" +
    "<div class='select2-result-repository__meta'>" +
      "<div class='select2-result-repository__title'>" + repo.value + "</div></div></div>";

  return markup;
}


function formatRepoSelection (repo) {
  return repo.value || repo.text;
}

</script>
