<!-- css style sheet ทั้งหมด -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/simple-line-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/datatables.bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/animate.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/fullcalendar.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/bootstrap-material-datetimepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/style.css')}}">

