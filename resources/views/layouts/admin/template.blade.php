<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
<meta charset="utf-8">
<meta name="expires" content="never" >
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta property="og:title" content="{{SettingWeb::SettingWeb()->Set_Title}}"/>
<meta property="og:image" content="{{asset('upload/admin/logo_web/'.SettingWeb::SettingWeb()->Set_Logo)}}"/>
<meta name="description" content="{{SettingWeb::SettingWeb()->Set_Description}}">
<meta name="keywords" content="{{SettingWeb::SettingWeb()->Set_Keywords}}">
<meta name="robots" content="{{SettingWeb::SettingWeb()->Set_Robots}}">

<title>{{SettingWeb::SettingWeb()->Set_Title}}</title>

<link rel="icon" href="{{asset('upload/admin/shortcut_icon/icon.ico')}}" type="image/x-icon" />

 @include('layouts.admin.stylesheet')
 @yield('stylesheet')

</head>

<body id="mimin" class="dashboard">

@include('layouts.admin.header')
@yield('header')

<div class="container-fluid mimin-wrapper">

@include('layouts.admin.menu_left')
@yield('menu_left')


@yield('detail')


@include('layouts.admin.footer')
@yield('footer')

</div>


@include('layouts.admin.javascripts')
@yield('javascript')


@include('layouts.admin.plugins')
@yield('plugins')


@include('layouts.admin.modal')
@yield('modal')


@include('layouts.admin.validation')
@yield('validation')

</body>
</html>