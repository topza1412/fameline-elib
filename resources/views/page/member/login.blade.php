@extends('layouts.member.template')

@section('detail')
        <div id="focus_div"> 
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="signin-main">
                        <div class="container">
                            <div class="woocommerce">
                                <div class="woocommerce-login">
                                    <div class="company-info signin-register">
                                        <div class="col-md-5 col-md-offset-1 border-dark-left">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="company-detail bg-dark margin-left">
                                                        <div class="signin-head">
                                                            <h2>Login</h2>
                                                            <span class="underline left"></span>
                                                        </div>
                                                        <form class="form-signin" method="post" action="{{url('login/auth')}}">
                                                          @csrf

                                                          @include('layouts.member.flash-message')

                                                            @if(count($errors))
                                                                <div class="alert alert-danger alert-block">
                                                                  <button type="button" class="close" data-dismiss="alert">×</button>
                                                                  <div style="padding: 10px;">
                                                                    <ul>
                                                                        @foreach($errors->all() as $error)
                                                                            <li>{{$error}}</li>
                                                                        @endforeach
                                                                    </ul>
                                                                  </div>
                                                                </div>
                                                           @endif

                                                            <p class="form-row form-row-first input-required">
                                                                <input type="text" id="username" name="username" class="input-text" required placeholder="Username" value="@if(isset($_COOKIE['remember_username'])){{$_COOKIE['remember_username']}}@endif">
                                                             </p>
                                                            <p class="form-row form-row-last input-required">
                                                                <input type="password" id="password" name="password" class="input-text" required placeholder="Password" value="@if(isset($_COOKIE['remember_password'])){{$_COOKIE['remember_password']}}@endif">
                                                            </p>
                                                            <div class="clear"></div>
                                                            <div class="password-form-row">
                                                                <p class="form-row">
                                                                    <input type="checkbox" value="1" id="remember_login" name="remember_login" @if(isset($_COOKIE['remember_login'])){{'checked'}}@endif>
                                                                    <label class="inline" for="rememberme" style="font-size: 18px; color: #fff;">Remember me</label>
                                                                </p>
                                                                <p class="lost_password" style="color: red;">
                                                                    <a href="{{url('forgotpassword')}}">Forgot password?</a>
                                                                </p>
                                                            </div>
                                                            <input type="submit" value="Login" name="login" class="button btn btn-default">
                                                            <div class="clear"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 border-dark new-user">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="company-detail new-account bg-light margin-right">
                                                        <div class="new-user-head">
                                                            <h2 class="bg-success alert">Create New Account</h2>
                                                            <span class="underline left"></span>
                                                            <p>The case of not being able to apply for membership, please contact the IT department.</p>
                                                        </div>
                                                        <form class="login" method="post" action="{{url('register')}}">
                                                            @csrf
                                                            <p class="form-row form-row-first input-required">
                                                                <label>
                                                                    <span class="first-letter">Full Name</span>  
                                                                    <span class="second-letter">*</span>
                                                                </label>
                                                                <input type="text" id="fullname" name="fullname" required class="input-text">
                                                            </p>
                                                            <p class="form-row form-row-first input-required">
                                                                <label>
                                                                    <span class="first-letter">Username</span>  
                                                                    <span class="second-letter">*</span>
                                                                </label>
                                                                <input type="text" id="use_username" name="use_username" required class="input-text">
                                                            </p>
                                                            <p class="form-row input-required">
                                                                <label>
                                                                    <span class="first-letter">Password</span>  
                                                                    <span class="second-letter">*</span>
                                                                </label>
                                                                <input type="password" id="password1" name="password" required class="input-text">
                                                            </p>  
                                                            <p class="form-row form-row-first input-required">
                                                                <label>
                                                                    <span class="first-letter">Email</span>  
                                                                    <span class="second-letter">*</span>
                                                                </label>
                                                                <input type="email" id="use_email" name="use_email" required class="input-text">
                                                            </p>         
                                                            <div class="clear"></div>
                                                            <input type="submit" value="Signup" name="signup" class="button btn btn-default">
                                                            <div class="clear"></div>
                                                        </form> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        </div>
@stop

