@extends('layouts.member.template')

@section('detail') 

<style type="text/css">
.acc-edit .open{
    padding-bottom: 0;
}
.acc-edit ul{
    margin-bottom: 0!important;
}
.acc-edit .open>ul{
    margin-top: 5px;
}
</style>

<div id="focus_div"> 
<div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="books-media-gird">
                        <div class="container">
                            <br>
                            <div class="row">
                                <div class="col-md-9 col-md-push-3">
                                    <h3>Favorit knowledge</h3>
                                    <hr>
                                    <div class="books-gird">
                                        <ul>
                                        @if(count($data['result'])>0)
                                            @foreach($data['result'] as $value)
                                                @if($value->Gro_ID == 1)
                                                    <?php $group = 'core';?>
                                                @elseif($value->Gro_ID == 2)
                                                    <?php $group = 'advance';?>
                                                @elseif($value->Gro_ID == 3)
                                                    <?php $group = 'innovation';?>
                                                @endif
                                            <li>
                                                <figure>
                                                    <img src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$value->Kno_Thumbnail)}}" alt="Book">
                                                    <figcaption>
                                                        <p><strong>{{$value->Kno_Title}}</strong></p>
                                                        <p><strong>Date:</strong> {{date("d-m-Y",strtotime($value->date_create))}}</p>
                                                        <p><strong>Author:</strong>  {{$value->Adm_Username}}</p>
                                                    </figcaption>
                                                </figure> 
                                                <div class="single-book-box">
                                                    <div class="post-detail">
                                                        <header class="entry-header">
                                                            <h3 class="entry-title"><a href="books-media-detail-v1.html">{{$value->Kno_Title}}</a></h3>
                                                            <ul>
                                                                <li><strong>Author:</strong> {{$value->Adm_Username}}</li>
                                                            </ul>
                                                        </header>
                                                        <div class="entry-content">
                                                            <p>{{$value->Kno_ShortContent}}</p>
                                                        </div>
                                                        <footer class="entry-footer">
                                                            <a class="btn btn-primary" href="{{url('knowledge/detail/'.$value->Kno_ID)}}">Read More</a>
                                                        </footer>
                                                    </div>
                                                </div>                                       
                                            </li>
                                            @endforeach
                                        @else
                                            <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                        @endif
                                        </ul>
                                    </div>
                                    <nav class="navigation pagination text-center">
                                        <div class="nav-links">
                                            {{$data['result']->links()}}
                                            <br>
                                        </div>
                                    </nav>
                                </div>
                                <div class="col-md-3 col-md-pull-9">
                                    <aside id="secondary" class="sidebar widget-area" data-accordion-group>
                                        <div class="widget widget_related_search open" data-accordion>
                                            <h4 class="widget-title" data-control>Main Menu</h4>
                                            <div data-content>
                                                <div data-accordion>
                                                    <h5 class="widget-sub-title" data-control>Core knowledge</h5>
                                                    <div class="widget_categories" data-content>
                                                        <ul class="acc-edit">
                                                        @if(count(SettingWeb::CategoryMenu()['menu_main']['core']) > 0)
                                                            @foreach(SettingWeb::CategoryMenu()['menu_main']['core'] as $key => $value)
                                                            @if($value->Cat_Submenu == 1)
                                                                <li data-accordion>
                                                                    <a data-control>{{$value->Cat_Name}}</a>
                                                                    <ul data-content>
                                                                        @foreach(SettingWeb::CategoryMenu()['menu_sub_level1']['core'] as $key2 => $value2)
                                                                        @if($value2->Suc1_Submenu == 1)
                                                                            <li data-accordion>
                                                                                <a data-control>{{$value2->Suc1_Name}}</a>
                                                                                <ul data-content>
                                                                                    @foreach(SettingWeb::CategoryMenu()['menu_sub_level2']['core'] as $key3 => $value3)
                                                                                    @if($value3->Suc1_ID == $value2->Suc1_ID)
                                                                                    <li><a href="{{url('category/3/'.$value3->Suc2_ID)}}">> {{$value3->Suc2_Name}}</a></li>
                                                                                    @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </li>
                                                                        @else
                                                                            <li><a href="{{url('category/2/'.$value2->Suc1_ID)}}">> {{$value2->Suc1_Name}}</a>
                                                                            </li>
                                                                        @endif
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                            @else
                                                                <li><a href="{{url('category/1/'.$value->Cat_ID)}}">> {{$value->Cat_Name}}</a>
                                                                </li>
                                                            @endif
                                                            @endforeach
                                                        @else
                                                        <div>No data not found!</div>
                                                        @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div data-accordion>
                                                    <h5 class="widget-sub-title" data-control>Advanced knowledge</h5>
                                                    <div class="widget_categories" data-content>
                                                        <ul class="acc-edit">
                                                            @if(count(SettingWeb::CategoryMenu()['menu_main']['advance']) > 0)
                                                                @foreach(SettingWeb::CategoryMenu()['menu_main']['advance'] as $key => $value)
                                                                @if($value->Cat_Submenu == 1)
                                                                    <li data-accordion>
                                                                        <a data-control>{{$value->Cat_Name}}</a>
                                                                        <ul data-content>
                                                                            @foreach(SettingWeb::CategoryMenu()['menu_sub_level1']['advance'] as $key2 => $value2)
                                                                            @if($value2->Suc1_Submenu == 1)
                                                                                <li data-accordion>
                                                                                    <a data-control>{{$value2->Suc1_Name}}</a>
                                                                                    <ul data-content>
                                                                                        @foreach(SettingWeb::CategoryMenu()['menu_sub_level2']['advance'] as $key3 => $value3)
                                                                                        @if($value3->Suc1_ID == $value2->Suc1_ID)
                                                                                        <li><a href="{{url('category/3/'.$value3->Suc2_ID)}}">> {{$value3->Suc2_Name}}</a></li>
                                                                                        @endif
                                                                                        @endforeach
                                                                                    </ul>
                                                                                </li>
                                                                            @else
                                                                                <li><a href="{{url('category/2/'.$value2->Suc1_ID)}}">> {{$value2->Suc1_Name}}</a>
                                                                                </li>
                                                                            @endif
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                @else
                                                                    <li><a href="{{url('category/1/'.$value->Cat_ID)}}">> {{$value->Cat_Name}}</a>
                                                                    </li>
                                                                @endif
                                                                @endforeach
                                                            @else
                                                            <div>No data not found!</div>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                @if(session('session_department') != 1 && session('session_department') != 2)
                                                    <div data-accordion>
                                                        <h5 class="widget-sub-title" data-control>Innovation knowledge</h5>
                                                        <div class="widget_categories" data-content>
                                                            <ul class="acc-edit">
                                                        @if(count(SettingWeb::CategoryMenu()['menu_main']['innovation']) > 0)
                                                            @foreach(SettingWeb::CategoryMenu()['menu_main']['innovation'] as $key => $value)
                                                            @if($value->Cat_Submenu == 1)
                                                                <li data-accordion>
                                                                    <a data-control>{{$value->Cat_Name}}</a>
                                                                    <ul data-content>
                                                                        @foreach(SettingWeb::CategoryMenu()['menu_sub_level1']['innovation'] as $key2 => $value2)
                                                                        @if($value2->Suc1_Submenu == 1)
                                                                            <li data-accordion>
                                                                                <a data-control>{{$value2->Suc1_Name}}</a>
                                                                                <ul data-content>
                                                                                    @foreach(SettingWeb::CategoryMenu()['menu_sub_level2']['innovation'] as $key3 => $value3)
                                                                                    @if($value3->Suc1_ID == $value2->Suc1_ID)
                                                                                    <li><a href="{{url('category/3/'.$value3->Suc2_ID)}}">> {{$value3->Suc2_Name}}</a></li>
                                                                                    @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </li>
                                                                        @else
                                                                            <li><a href="{{url('category/2/'.$value2->Suc1_ID)}}">> {{$value2->Suc1_Name}}</a>
                                                                            </li>
                                                                        @endif
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                            @else
                                                                <li><a href="{{url('category/1/'.$value->Cat_ID)}}">> {{$value->Cat_Name}}</a>
                                                                </li>
                                                            @endif
                                                            @endforeach
                                                        @else
                                                        <div>No data not found!</div>
                                                        @endif
                                                        </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div data-accordion>
                                                    <h5 class="widget-sub-title" data-control>Setting</h5>
                                                    <div class="widget_categories" data-content>
                                                        <ul>
                                                            <li><a href="{{url('changepassword')}}">Change Password</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                    <h4 style="font-size: 24px !important;"><a href="{{url('logout')}}">Logout</a></h4>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
</div>
</div>
@stop

