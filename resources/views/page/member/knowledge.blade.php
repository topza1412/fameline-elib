@extends('layouts.member.template')

@section('detail') 
<div id="focus_div"> 
<div id="content" class="site-content">
    <div id="primary" class="content-area">
    <main id="main" class="site-main">
    <div class="blog-main-list" style="background-color: #e5e5e5; background-size:cover;">
    <div class="container">
    <div class="row">
    <div class="blog-page grid">
    <h2 align="center" style="color:#231f20;">Search data Knowledge</h2>
    <p align="center"><b>Keyword search:</b> <span class="text-success">{{$data['keyword']}}</span></p>
    @if(count($data['result'])>0)
        @foreach($data['result'] as $value)
            @if($value->Gro_ID == 1)
                <?php $group = 'core';?>
            @elseif($value->Gro_ID == 2)
                <?php $group = 'advance';?>
            @elseif($value->Gro_ID == 3)
                <?php $group = 'innovation';?>
            @endif 
            <article>
                <div class="grid-item blog-item">
                    <div class="post-thumbnail">
                        <div class="post-date-box">
                            <div class="post-date">
                                <a class="date" href="#.">{{date("d",strtotime($value->date_create))}}</a>
                            </div>
                            <div class="post-date-month">
                                <a class="month" href="#.">{{date("M",strtotime($value->date_create))}}</a>
                            </div>
                        </div>
                        <a href="{{url('knowledge/detail/'.$value->Kno_ID)}}"><img alt="blog" src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$value->Kno_Thumbnail)}}" /></a>
                        <div class="post-share">
                            <a href="#."><i class="fa fa-thumbs-o-up"></i> {{$value->Kno_Like}}</a>
                            <a href="#."><i class="fa fa-eye"></i> {{$value->Kno_View}}</a>
                        </div>
                    </div>
                    <div class="post-detail">
                        <header class="entry-header">
                            <h3 class="entry-title"><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a></h3>
                        </header>
                        <div class="entry-content">
                            <p>{{$value->Kno_ShortContent}}</p>
                        </div>
                    </div>
                </div>
            </article>
        @endforeach
    @else
        <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
    @endif
        <div class="col-md-12 alert" align="center">{{$data['result']->links()}}<br></div>
    </div>
    </div>
    </div>
    </div>
    </main>
    </div>
</div>
</div>
@stop

