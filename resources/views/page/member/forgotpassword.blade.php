@extends('layouts.member.template')

@section('detail') 
<div id="focus_div"> 
<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="signin-main">
                <div class="container">
                    <div class="woocommerce">
                        <div class="woocommerce-login">
                            <div class="company-info signin-register">
                                <div class="col-md-5 col-md-offset-1 border-dark-left">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="company-detail bg-dark margin-left">
                                                <div class="signin-head">
                                                    <h2>Forgot password</h2>
                                                    <span class="underline left"></span>
                                                </div>
                                                @if($data['step']=='step1')
                                                  <form class="form-signin" method="post" action="{{url('forgotpassword/sendmail')}}">
                                                    @csrf

                                                    @include('layouts.member.flash-message')

                                                      @if(count($errors))
                                                          <div class="alert alert-danger alert-block">
                                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                                            <div style="padding: 10px;">
                                                              <ul>
                                                                  @foreach($errors->all() as $error)
                                                                      <li>{{$error}}</li>
                                                                  @endforeach
                                                              </ul>
                                                            </div>
                                                          </div>
                                                     @endif

                                                      <p class="form-row form-row-first input-required">
                                                          <input type="text" id="username" name="username" class="input-text" required placeholder="Username">
                                                       </p>
                                                      <p class="form-row form-row-last input-required">
                                                          <input type="password" id="email" name="email" class="input-text" required placeholder="Email">
                                                      </p>
                                                      <div class="clear"></div>
                                                      <input type="submit" value="Confirm" name="Confirm" class="button btn btn-default">
                                                      <div class="clear"></div>
                                                  </form>
                                                @else
                                                <form class="form-signin" method="post" action="{{url('forgotpassword/verify')}}">
                                                    @csrf

                                                    @include('layouts.member.flash-message')

                                                      @if(count($errors))
                                                          <div class="alert alert-danger alert-block">
                                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                                            <div style="padding: 10px;">
                                                              <ul>
                                                                  @foreach($errors->all() as $error)
                                                                      <li>{{$error}}</li>
                                                                  @endforeach
                                                              </ul>
                                                            </div>
                                                          </div>
                                                     @endif

                                                     <input type="hidden" name="token_email" value="{{$data['token_email']}}">

                                                      <p class="form-row form-row-first input-required">
                                                          <input type="password" id="password" name="username" class="input-text" required placeholder="Password">
                                                       </p>
                                                      <p class="form-row form-row-last input-required">
                                                          <input type="password" id="password_confirmation" name="password_confirmation" class="input-text" required placeholder="Confirm password">
                                                      </p>
                                                      <div class="clear"></div>
                                                      <input type="submit" value="Confirm" name="Confirm" class="button btn btn-default">
                                                      <div class="clear"></div>
                                                  </form>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 border-dark new-user">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="company-detail new-account bg-light margin-right">
                                                <div class="new-user-head">
                                                  <h2 style="color:red;">Forgot password!</h2>
                                                  <br>
                                                  In case of forgetting the password for access, you can notify the forgotten password on the form on the left. After that, the system will send the new password setting details to your Email!
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
</div>
@stop

