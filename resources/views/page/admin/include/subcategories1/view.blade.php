<div class="responsive-table">

@if($data['page'] == 'core')
<?php $group = 'Core knowledge';?>
@elseif($data['page'] == 'advance')
<?php $group = 'Advance knowledge';?>
@elseif($data['page'] == 'innovation')
<?php $group = 'Innovation knowledge';?>
@endif

      <a href="{{url('admin/subcategories1/'.$data['page'].'/add')}}" class="btn btn-warning"><i class="fa fa-edit f-s-15"></i> New+</a>
      <br><br>
      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
      <thead>
        <tr>
        <th>#</th>
        <th>Sub category level1</th>
        <th>Category</th>
        <th>Group</th>
        <th>Date create</th>
        <th>Manage</th>
        </tr>
      </thead>
      <tbody>
        @if(count($data['result'])>0)
            <?php $i = 1;?>
            @foreach($data['result'] as $value)
            <tr>
            <td>{{$i++}}</td>
            <td>{{$value->Suc1_Name}}</td>
            <td>{{$value->Cat_Name}}</td>
            <td>{{$group}}</td>
            <td>{{date("d-m-Y",strtotime($value->date_create))}}</td>
            <td>
            <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="fa fa-pencil-square-o"></span> Action
              <span class="fa fa-angle-down"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="{{url('admin/subcategories1/'.$data['page'].'/edit/'.$value->Suc1_ID)}}" class="dropdown-item">Edit</a></li>
              <li><a href="#" class="dropdown-item" onclick="confirm_delete('{{url('admin/subcategories1/delete/group')}}','{{$value->Suc1_ID}}');">Delete</a></li>
            </ul>
           </div>
            </td>
            </tr>
            @endforeach
        @else
            <tr>
            <td colspan="10" class="text-danger"><div align="center">No data not found!</div></td>    
            </tr>
        @endif
       </tbody>
       </table>

</div>

