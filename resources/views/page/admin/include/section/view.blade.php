<div class="responsive-table">

    @if($data['page'] == 'group')

        <a href="{{url('admin/section/'.$data['page'].'/add')}}" class="btn btn-warning"><i class="fa fa-edit f-s-15"></i> New+</a>
        <br><br>
        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
          <tr>
          <th>#</th>
          <th>Section Name</th>
          <th>Group</th>
          <th>Status</th>
          <th>Date create</th>
          <th>Manage</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data['result'])>0)
              <?php $i = 1;?>
              @foreach($data['result'] as $value)
                @if($value->Gro_ID == 1)
                <?php $group = 'Core knowledge';?>
                @elseif($value->Gro_ID == 2)
                  <?php $group = 'Advance knowledge';?>
                @elseif($value->Gro_ID == 3)
                  <?php $group = 'Innovation knowledge';?>
                @endif
              <tr>
              <td>{{$i++}}</td>
              <td>{{$value->Sec_Name}}</td>
              <td>{{$group}}</td>
              <td>
              @if($value->Sec_Status==1)
              <span class="badge badge-success">Active</span>
              @else
              <span class="badge badge-danger">In-Active</span>
              @endif
              </td>
              <td>{{$value->created_at}}</td>
              <td>
              <div class="btn-group" role="group">
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-pencil-square-o"></span> Action
                <span class="fa fa-angle-down"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="{{url('admin/section/detail?id='.$value->Sec_ID)}}" class="dropdown-item">Detail</a></li>
                <li><a href="{{url('admin/section/'.$data['page'].'/edit/'.$value->Sec_ID)}}" class="dropdown-item">Edit</a></li>
                <li><a href="#" class="dropdown-item" onclick="confirm_delete('{{url('admin/section/delete/group')}}','{{$value->Sec_ID}}');">Delete</a></li>
              </ul>
             </div>
              </td>
              </tr>
              @endforeach
          @else
              <tr>
              <td colspan="8" class="text-danger"><div align="center">No data not found!</div></td>    
              </tr>
          @endif
         </tbody>
         </table>

    @else

        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
          <tr>
          <th>#</th>
          <th>Section Name</th>
          <th>Knowledge Name</th>
          <th>Group</th>
          <th>Category</th>
          <th>Manage</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data['result'])>0)
              <?php $i = 1;?>
              @foreach($data['result'] as $value)
                @if($value->Gro_ID == 1)
                <?php $group = 'Core knowledge';?>
                @elseif($value->Gro_ID == 2)
                  <?php $group = 'Advance knowledge';?>
                @elseif($value->Gro_ID == 3)
                  <?php $group = 'Innovation knowledge';?>
                @endif
              <tr>
              <td>{{$i++}}</td>
              <td>{{$value->Sec_Name}}</td>
              <td>{{$value->Kno_Title}}</td>
              <td>{{$group}}</td>
              <td>{{$value->Cat_Name}}</td>
              <td>
              <div class="btn-group" role="group">
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-pencil-square-o"></span> Action
                <span class="fa fa-angle-down"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a target="_blank" href="{{url('knowledge/detail/'.$value->Kno_ID)}}" class="dropdown-item">View</a></li>
                <li><a href="#" class="dropdown-item" onclick="confirm_delete('{{url('admin/section/delete/knowledge')}}','{{$value->Sed_ID}}');">Delete</a></li>
              </ul>
             </div>
              </td>
              </tr>
              @endforeach
          @else
              <tr>
              <td colspan="6" class="text-danger"><div align="center">No data not found!</div></td>    
              </tr>
          @endif
         </tbody>
         </table>

    @endif

</div>

