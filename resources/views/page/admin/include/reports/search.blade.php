<div class="responsive-table">

      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">

          @if($data['page']=='logknowledge')
              <thead>
                <tr class="small">
                <th>#</th>
                <th>Knowledge</th>
                <th>Group</th>
                <th>Category</th>
                <th>Admin create</th>
                <th>IP</th>
                <th>Date create</th>
                </tr>
              </thead>
              <tbody>
                @if(count($data['result'])>0)
                    <?php $i = 1;?>
                    @foreach($data['result'] as $value)
                        @if($value->Gro_ID == 1)
                            <?php $group = 'Core knowledge';?>
                        @elseif($value->Gro_ID == 2)
                            <?php $group = 'Advance knowledge';?>
                        @elseif($value->Gro_ID == 3)
                            <?php $group = 'Innovation knowledge';?>
                        @endif
                        <tr class="small">
                        <td>{{$i++}}</td>
                        <td>{{$value->Kno_Title}}</td>
                        <td>{{$group}}</td>
                        <td>{{$value->Cat_Name}}</td>
                        <td>{{$value->Adm_Username}}</td>
                        <td>{{$value->IP}}</td>
                        <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>                        
                        </tr>
                    @endforeach
                @else
                    <tr>
                    <td colspan="7" class="text-danger"><div align="center">No data not found!</div></td>    
                    </tr>
                @endif
              </tbody>
            @endif

            @if($data['page']=='logsystem')
              <thead>
                <tr class="small">
                <th>#</th>
                <th>Name</th>
                <th>Detail</th>
                <th>Url access</th>
                <th>Browser access</th>
                <th>IP</th>
                <th>Date Create</th>
                </tr>
              </thead>
              <tbody>
                @if(count($data['result'])>0)
                    <?php $i = 1;?>
                    @foreach($data['result'] as $value)
                        <tr class="small">
                        <td>{{$i++}}</td>
                        <td>{{$value->Log_Name}}</td>
                        <td>{{$value->Log_Detail}}</td>
                        <td>{{$value->Log_UrlAccess}}</td>
                        <td>{{$value->Log_Browser}}</td>
                        <td>{{$value->Log_IP}}</td>
                        <td>{{$value->Adm_Username}}</td>
                        <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>                        
                        </tr>
                    @endforeach
                @else
                    <tr>
                    <td colspan="7" class="text-danger"><div align="center">No data not found!</div></td>    
                    </tr>
                @endif
              </tbody>
            @endif

            @if($data['page']=='member')
              <thead>
                <tr class="small">
                <th>#</th>
                <th>Full Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Tel</th>
                <th>Department</th>
                <th>Permission</th>
                <th>Date Create</th>
                </tr>
              </thead>
              <tbody>
                @if(count($data['result'])>0)
                    <?php $i = 1;?>
                    @foreach($data['result'] as $value)
                        @if($value->Adm_Department == 1)
                            <?php $department = 'Marketing';?>
                        @elseif ($value->Adm_Department == 2)
                            <?php $department = 'HR';?>
                        @elseif ($value->Adm_Department == 3)
                            <?php $department = 'R&D';?>
                        @elseif ($value->Adm_Department == 4)
                            <?php $department = 'Production';?>
                        @endif
                        <tr class="small">
                        <td>{{$i++}}</td>
                        <td>{{$value->Use_Fullname}}</td>
                        <td>{{$value->Use_Username}}</td>
                        <td>{{$value->Use_Email}}</td>
                        <td>{{$value->Use_Tel}}</td>
                        <td>{{$department}}</td>
                        <td>
                        @if($value->Adm_Permission==1)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">In-Active</span>
                        @endif
                       </td>
                        <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>                        
                        </tr>
                    @endforeach
                @else
                    <tr>
                    <td colspan="8" class="text-danger"><div align="center">No data not found!</div></td>    
                    </tr>
                @endif
              </tbody>
            @endif

            @if($data['page']=='visitweb')
              <thead>
                <tr class="small">
                <th>#</th>
                <th>IP</th>
                <th>Browser</th>
                <th>Device</th>
                <th>Date Visit</th>
                </tr>
              </thead>
              <tbody>
                @if(count($data['result'])>0)
                    <?php $i = 1;?>
                    @foreach($data['result'] as $value)
                        <tr class="small">
                        <td>{{$i++}}</td>
                        <td>{{$value->Viw_IP}}</td>
                        <td>{{$value->Viw_Browser}}</td>
                        <td>{{$value->Viw_Device}}</td>
                        <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>                        
                        </tr>
                    @endforeach
                @else
                    <tr>
                    <td colspan="5" class="text-danger"><div align="center">No data not found!</div></td>    
                    </tr>
                @endif
               </tbody>
            @endif

       </table>

</div>
