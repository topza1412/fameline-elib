<div class="responsive-table">

        <form action="{{url('admin/reports/'.$data['page'].'/search')}}" method="post">
        @csrf

                <div class="row">

                <div class="col-lg-5">
                <div class="form-group form-animate-text">
                <input type="text" name="date_start" class="form-text datepickers" required>
                <span class="bar"></span>
                <label><span class="fa fa-calendar"></span> Date Start</label>
                </div>
                </div>

                <div class="col-lg-5">
                <div class="form-group form-animate-text">
                <input type="text" name="date_end" class="form-text datepickers" required>
                <span class="bar"></span>
                <label><span class="fa fa-calendar"></span> Date End</label>
                </div>
                </div>

                <div class="col-lg-12">
                <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Report">
                </div>
                </div>

                </div>

        </form>

</div>
