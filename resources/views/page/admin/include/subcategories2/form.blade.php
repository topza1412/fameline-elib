<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/subcategories2/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data" autocomplete="false">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['id'])){{$data['id']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                            @if(isset($data['result']['Suc2_Name']))
                                <?php $sub_cate2 = $data['result']['Suc2_Name'];?>
                            @else
                                <?php $sub_cate2 = null;?>
                            @endif

                            @if(isset($data['result']['Suc1_ID']))
                                <?php $sub_cate1 = $data['result']['Suc1_ID'];?>
                            @else
                                <?php $sub_cate1 = null;?>
                            @endif


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Sub category level 1 <span class="text-danger">*</span></label>
                                    <select name="sub_cate1" class="form-control" required>
                                      <option value="">Select</option>
                                      @foreach($data['main_cate'] as $value)
                                        <option value="{{$value->Suc1_ID}}" @if($sub_cate1 == $value->Suc1_ID){{'selected'}}@endif>{{$value->Suc1_Name}}</option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="sub_cate2" type="text" class="form-text" required value="{{$sub_cate2}}">
                                    <label>Sub category level 2</label>
                                </div>
                                @if($data['action']=='update')
                                <div class="form-group">
                                    <input name="permission" type="radio" required @if($data['result']['Suc2_Status'] == 1){{'checked'}}@endif value="1"> Active
                                    <input name="permission" type="radio" required @if($data['result']['Suc2_Status'] == 0){{'checked'}}@endif value="0"> In-Active
                                </div>
                                @endif

                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>