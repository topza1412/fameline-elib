<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/setting/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['detail']['Sli_ID'])){{$data['detail']['Sli_ID']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                        @if($data['page']=='slider')

                        @if(isset($data['detail']['Sli_Name']))
                            <?php $name = $data['detail']['Sli_Name'];?>
                            @else
                            <?php $name = null;?>
                        @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                        <input name="name" type="text" class="form-text" required value="{{$name}}">
                                        <label>Name slide</label>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Img slide</label>
                                        @if(isset($data['detail']['Sli_Img']))
                                        <br>
                                        <input type="hidden" name="slide_img_hidden" value="{{$data['detail']['Sli_Img']}}">
                                        <img src="{{asset('upload/member/slide/'.$data['detail']['Sli_Img'])}}" width="250">
                                        @endif
                                        <input name="slide_img" type="file" class="form-control" placeholder="Image Slide">
                                        <p class="small text-danger">*Image size not over 2 mb. 1280*650 || Support only file jpeg,jpg,png</p>
                                    </div>
                                    @if($data['action']=='update')
                                    <div class="form-group">
                                        <label>Status</label>
                                        <br>
                                        <input name="status" type="radio" required @if($data['detail']['Sli_Status']==1){{'checked'}}@endif value="1"> Active
                                        <input name="status" type="radio" required @if($data['detail']['Sli_Status']==0){{'checked'}}@endif value="0"> In-Active
                                    </div>
                                    @endif

                                </div>

                                <div class="col-lg-12">

                                    <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                    &nbsp;
                                    <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                                </div>

                            </div>
                        @endif

                    </form>

                </div>
            </div>