@extends('layouts.admin.template')

@section('detail') 

<div class="container-fluid mimin-wrapper">

<!-- start: content -->
<div id="content">
    <div class="col-md-12" style="padding:20px;">
        <div class="col-md-12 padding-0">
            <div class="col-md-12 padding-0">
                <div class="col-md-12">
                    <div class="panel box-v4">
                        <div class="panel-heading bg-white border-none">
                          <h4><span class="fa fa-area-chart"></span> {{$data['title']->Kno_Title}}</h4>
                        </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              <div class="responsive-table">
                                <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                                <thead>
                                  <tr class="small">
                                  <th>View</th>
                                  <th>Like</th>
                                  <th>Comment</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <td>{{$data['result']['view']->Kno_View}}</td>
                                  <td>{{$data['result']['like']->Kno_Like}}</td>
                                  <td>{{$data['result']['comment']}}</td>
                                </tbody>
                                </table>
                              </div>
                            </div>
                            
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  </div>
<!-- end: content -->

@stop

