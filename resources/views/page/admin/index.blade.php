@extends('layouts.admin.template')

@section('detail') 

<div class="container-fluid mimin-wrapper">

<!-- start: content -->
<div id="content">
    <div class="col-md-12" style="padding:20px;">
        <div class="col-md-12 padding-0">
            <div class="col-md-12 padding-0">
                <div class="col-md-12 padding-0">
                    <div class="col-md-3">
                        <div class="panel box-v1">
                          <div class="panel-heading bg-white border-none">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                              <h4 class="text-left">Member</h4>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                               <h4>
                               <span class="icon-user icons icon text-right"></span>
                               </h4>
                            </div>
                          </div>
                          <div class="panel-body text-center">
                            <h1>@if($data['count_data']['count_member']>0) {{$data['count_data']['count_member']}} @else {{0}} @endif</h1>
                            <hr/>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel box-v1">
                          <div class="panel-heading bg-white border-none">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                              <h4 class="text-left">Core Knowledge</h4>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                               <h4>
                               <span class="fa fa-book text-right"></span>
                               </h4>
                            </div>
                          </div>
                          <div class="panel-body text-center">
                            <h1>@if($data['count_data']['count_core_knowledge']>0) {{$data['count_data']['count_core_knowledge']}} @else {{0}} @endif</h1>
                            <hr/>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel box-v1">
                          <div class="panel-heading bg-white border-none">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                              <h4 class="text-left">Advance Knowledge</h4>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                               <h4>
                               <span class="fa fa-book text-right"></span>
                               </h4>
                            </div>
                          </div>
                          <div class="panel-body text-center">
                            <h1>@if($data['count_data']['count_advance_knowledge']>0) {{$data['count_data']['count_advance_knowledge']}} @else {{0}} @endif</h1>
                            <hr/>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel box-v1">
                          <div class="panel-heading bg-white border-none">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                              <h4 class="text-left">Innovation Knowledge</h4>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                               <h4>
                               <span class="fa fa-book text-right"></span>
                               </h4>
                            </div>
                          </div>
                          <div class="panel-body text-center">
                            <h1>@if($data['count_data']['count_innovation_knowledge']>0) {{$data['count_innovation_knowledge']['count_news']}} @else {{0}} @endif</h1>
                            <hr/>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel box-v4">
                        <div class="panel-heading bg-white border-none">
                          <h4><span class="fa fa-area-chart"></span> Chart visit web</h4>
                        </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['desktop']->html() !!}
                            </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['mobile']->html() !!}
                            </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['tablet']->html() !!}
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  </div>
<!-- end: content -->

{!! Charts::scripts() !!}
{!! $data['chart']['desktop']->script() !!}
{!! $data['chart']['mobile']->script() !!}
{!! $data['chart']['tablet']->script() !!}

@stop

