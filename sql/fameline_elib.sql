-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 18, 2020 at 07:17 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fameline_elib`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `Adm_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_Username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Department` int(11) NOT NULL DEFAULT 0,
  `Adm_Permission` int(11) NOT NULL DEFAULT 1 COMMENT '0 = ยกเลิก 1 = ใช้งาน',
  `Adm_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = admin 1 = super admin',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Adm_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Adm_ID`, `Adm_Username`, `Adm_Password`, `Adm_Fullname`, `Adm_Email`, `Adm_Tel`, `Adm_Department`, `Adm_Permission`, `Adm_Status`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'e10adc3949ba59abbe56e057f20f883e', 'Administrator', 'admin@g.com', '1234567890', 0, 1, 1, '2019-09-09 13:47:32', '2020-07-22 15:21:58'),
(2, 'adminmk', 'e10adc3949ba59abbe56e057f20f883e', 'admin mk', 'adminmk@fameline.com', '1234567890', 1, 1, 0, '2020-07-29 15:14:03', '2020-07-29 15:14:03');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Gro_ID` int(11) NOT NULL DEFAULT 0,
  `Cat_Name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cat_Submenu` int(11) NOT NULL DEFAULT 0 COMMENT '0 = no sub menu 1 = have sub menu',
  `Cat_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Cat_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Cat_ID`, `Gro_ID`, `Cat_Name`, `Cat_Submenu`, `Cat_Status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Category1', 1, 1, '2020-07-20 10:58:00', '2020-09-02 16:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `comment_knowledge`
--

DROP TABLE IF EXISTS `comment_knowledge`;
CREATE TABLE IF NOT EXISTS `comment_knowledge` (
  `Com_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kno_ID` int(11) DEFAULT 0,
  `Use_ID` int(11) DEFAULT 0,
  `Com_Detail` text COLLATE utf8_unicode_ci NOT NULL,
  `Com_IP` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Com_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment_knowledge`
--

INSERT INTO `comment_knowledge` (`Com_ID`, `Kno_ID`, `Use_ID`, `Com_Detail`, `Com_IP`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'test', '::1', '2020-08-26 14:44:56', '2020-08-26 14:44:56'),
(2, 1, 1, '5555', '::1', '2020-08-26 14:45:15', '2020-08-26 14:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_knowledge`
--

DROP TABLE IF EXISTS `favorite_knowledge`;
CREATE TABLE IF NOT EXISTS `favorite_knowledge` (
  `Fav_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kno_ID` int(11) NOT NULL DEFAULT 0,
  `Use_ID` int(11) NOT NULL DEFAULT 0,
  `Fav_IP` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`Fav_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorite_knowledge`
--

INSERT INTO `favorite_knowledge` (`Fav_ID`, `Kno_ID`, `Use_ID`, `Fav_IP`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '::1', '2020-09-02 16:48:43', '2020-09-02 16:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `knowledge`
--

DROP TABLE IF EXISTS `knowledge`;
CREATE TABLE IF NOT EXISTS `knowledge` (
  `Kno_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_ID` int(11) NOT NULL DEFAULT 0,
  `Gro_ID` int(11) NOT NULL DEFAULT 0,
  `Cat_ID` int(11) NOT NULL DEFAULT 0,
  `Suc1_ID` int(11) DEFAULT NULL,
  `Suc2_ID` int(11) DEFAULT NULL,
  `Sec_ID` int(11) DEFAULT 0,
  `Kno_Thumbnail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_ShortContent` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_FullContent` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Tags` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Galleries` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_VideoFile` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_DocsFile` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_View` int(11) NOT NULL DEFAULT 0,
  `Kno_Like` int(11) NOT NULL DEFAULT 0,
  `Kno_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Kno_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `knowledge`
--

INSERT INTO `knowledge` (`Kno_ID`, `Adm_ID`, `Gro_ID`, `Cat_ID`, `Suc1_ID`, `Suc2_ID`, `Sec_ID`, `Kno_Thumbnail`, `Kno_Title`, `Kno_ShortContent`, `Kno_FullContent`, `Kno_Tags`, `Kno_Galleries`, `Kno_VideoFile`, `Kno_DocsFile`, `Kno_View`, `Kno_Like`, `Kno_Status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '240720_163845.jpg', 'knowledge1', 'test', '<p>tttttttt</p>', '[\"test\",\"test2\",\"test3\"]', '{\"img_gallery1\":\"310720_151606.jpg\",\"img_gallery2\":null,\"img_gallery3\":null,\"img_gallery4\":null,\"img_gallery5\":null,\"img_gallery6\":null,\"img_gallery7\":null,\"img_gallery8\":null,\"img_gallery9\":null,\"img_gallery10\":null}', '070820_153257.mp4', '{\"docs1\":null,\"docs2\":null,\"docs3\":null}', 144, 3, 1, '2020-07-24 16:38:45', '2020-09-18 14:07:10');

-- --------------------------------------------------------

--
-- Table structure for table `logs_knowledge`
--

DROP TABLE IF EXISTS `logs_knowledge`;
CREATE TABLE IF NOT EXISTS `logs_knowledge` (
  `Log_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_ID` int(11) NOT NULL DEFAULT 0,
  `Kno_ID` int(11) NOT NULL DEFAULT 0,
  `Log_IP` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Log_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logs_knowledge`
--

INSERT INTO `logs_knowledge` (`Log_ID`, `Adm_ID`, `Kno_ID`, `Log_IP`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '::1', '2020-08-07 15:27:17', '2020-08-07 15:27:17'),
(2, 1, 1, '::1', '2020-08-07 15:32:57', '2020-08-07 15:32:57'),
(3, 1, 1, '::1', '2020-08-26 17:09:52', '2020-08-26 17:09:52'),
(4, 1, 1, '::1', '2020-09-18 12:30:30', '2020-09-18 12:30:30'),
(5, 1, 1, '::1', '2020-09-18 12:30:42', '2020-09-18 12:30:42'),
(6, 1, 1, '::1', '2020-09-18 12:43:25', '2020-09-18 12:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `logs_system`
--

DROP TABLE IF EXISTS `logs_system`;
CREATE TABLE IF NOT EXISTS `logs_system` (
  `Log_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Log_Name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Log_Detail` text COLLATE utf8_unicode_ci NOT NULL,
  `Log_UrlAccess` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Log_Browser` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Log_IP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_ID` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Log_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logs_system`
--

INSERT INTO `logs_system` (`Log_ID`, `Log_Name`, `Log_Detail`, `Log_UrlAccess`, `Log_Browser`, `Log_IP`, `Adm_ID`, `created_at`, `updated_at`) VALUES
(1, 'update data section', 'section1', 'http://famelineelib.master/admin/section/group/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-03 13:45:06', '2020-08-03 13:45:06'),
(2, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-05 11:41:50', '2020-08-05 11:41:50'),
(3, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-07 15:09:42', '2020-08-07 15:09:42'),
(4, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-07 15:27:17', '2020-08-07 15:27:17'),
(5, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-07 15:32:57', '2020-08-07 15:32:57'),
(6, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-10 15:13:42', '2020-08-10 15:13:42'),
(7, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 13:43:59', '2020-08-26 13:43:59'),
(8, 'insert data section', 'section2', 'http://famelineelib.master/admin/section/group/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 14:12:33', '2020-08-26 14:12:33'),
(9, 'report data report', 'logknowledge', 'http://famelineelib.master/admin/reports/logknowledge', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 14:14:09', '2020-08-26 14:14:09'),
(10, 'report data report', 'logsystem', 'http://famelineelib.master/admin/reports/logsystem', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 14:16:51', '2020-08-26 14:16:51'),
(11, 'report data report', 'visitweb', 'http://famelineelib.master/admin/reports/visitweb', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 14:28:05', '2020-08-26 14:28:05'),
(12, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 16:28:23', '2020-08-26 16:28:23'),
(13, 'insert data setting web', 'Fameline E-library', 'http://famelineelib.master/admin/setting/website', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 16:28:35', '2020-08-26 16:28:35'),
(14, 'insert data setting web', 'Fameline E-library', 'http://famelineelib.master/admin/setting/website', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 16:28:54', '2020-08-26 16:28:54'),
(15, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 16:58:44', '2020-08-26 16:58:44'),
(16, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', 1, '2020-08-26 17:09:52', '2020-08-26 17:09:52'),
(17, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-01 17:07:24', '2020-09-01 17:07:24'),
(18, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:04:47', '2020-09-02 16:04:47'),
(19, 'insert data sub categories level 1', 'sub cate level1', 'http://famelineelib.master/admin/subcategories1/core/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:07:48', '2020-09-02 16:07:48'),
(20, 'insert data sub categories level 1', 'sub cate level1', 'http://famelineelib.master/admin/subcategories1/core/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:10:21', '2020-09-02 16:10:21'),
(21, 'insert data sub categories level 1', 'sub cate level1', 'http://famelineelib.master/admin/subcategories1/core/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:13:43', '2020-09-02 16:13:43'),
(22, 'insert data sub categories level 2', 'sub cate level2', 'http://famelineelib.master/admin/subcategories2/core/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:26:40', '2020-09-02 16:26:40'),
(23, 'update data sub categories level 1', 'sub cate level1', 'http://famelineelib.master/admin/subcategories1/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-02 16:31:27', '2020-09-02 16:31:27'),
(24, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-08 12:30:11', '2020-09-08 12:30:11'),
(25, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-10 11:53:19', '2020-09-10 11:53:19'),
(26, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 11:41:30', '2020-09-18 11:41:30'),
(27, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 11:53:44', '2020-09-18 11:53:44'),
(28, 'login data login success', 'superadmin', 'http://famelineelib.master/admin/login', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 12:03:22', '2020-09-18 12:03:22'),
(29, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 12:30:30', '2020-09-18 12:30:30'),
(30, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 12:30:42', '2020-09-18 12:30:42'),
(31, 'update data knowledge', 'knowledge1', 'http://famelineelib.master/admin/knowledge/core/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', 1, '2020-09-18 12:43:25', '2020-09-18 12:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_admin`
--

DROP TABLE IF EXISTS `role_admin`;
CREATE TABLE IF NOT EXISTS `role_admin` (
  `Roa_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_ID` int(11) NOT NULL DEFAULT 0,
  `Roa_All` int(11) NOT NULL DEFAULT 0,
  `Roa_Member` int(11) NOT NULL DEFAULT 0,
  `Roa_KnowledgeAll` int(11) NOT NULL DEFAULT 0,
  `Roa_KnowledgeCore` int(11) NOT NULL DEFAULT 0,
  `Roa_KnowledgeAdvance` int(11) NOT NULL DEFAULT 0,
  `Roa_KnowledgeInnovation` int(11) NOT NULL DEFAULT 0,
  `Roa_CategoryAll` int(11) NOT NULL DEFAULT 0,
  `Roa_CategoryCore` int(11) NOT NULL DEFAULT 0,
  `Roa_CategoryAdvance` int(11) NOT NULL DEFAULT 0,
  `Roa_CategoryInnovation` int(11) NOT NULL DEFAULT 0,
  `Roa_Section` int(11) NOT NULL DEFAULT 0,
  `Roa_ReportsAll` int(11) NOT NULL DEFAULT 0,
  `Roa_ReportsLogKnowledge` int(11) NOT NULL DEFAULT 0,
  `Roa_ReportsLogSystem` int(11) NOT NULL DEFAULT 0,
  `Roa_ReportsMember` int(11) NOT NULL DEFAULT 0,
  `Roa_ReportsVisit` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Roa_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_admin`
--

INSERT INTO `role_admin` (`Roa_ID`, `Adm_ID`, `Roa_All`, `Roa_Member`, `Roa_KnowledgeAll`, `Roa_KnowledgeCore`, `Roa_KnowledgeAdvance`, `Roa_KnowledgeInnovation`, `Roa_CategoryAll`, `Roa_CategoryCore`, `Roa_CategoryAdvance`, `Roa_CategoryInnovation`, `Roa_Section`, `Roa_ReportsAll`, `Roa_ReportsLogKnowledge`, `Roa_ReportsLogSystem`, `Roa_ReportsMember`, `Roa_ReportsVisit`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2020-07-29 15:14:03', '2020-07-29 16:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `Sec_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sec_Name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gro_ID` int(11) NOT NULL DEFAULT 0,
  `Sec_Status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Sec_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`Sec_ID`, `Sec_Name`, `Gro_ID`, `Sec_Status`, `created_at`, `updated_at`) VALUES
(1, 'section1', 3, 1, '2020-07-30 15:42:42', '2020-08-03 13:45:06'),
(2, 'section2', 3, 1, '2020-08-26 14:12:33', '2020-08-26 14:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `section_detail`
--

DROP TABLE IF EXISTS `section_detail`;
CREATE TABLE IF NOT EXISTS `section_detail` (
  `Sed_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sec_ID` int(11) NOT NULL DEFAULT 0,
  `Kno_ID` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Sed_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `Set_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Set_Title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Logo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_About` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_EmailAdmin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Robots` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_StatusWeb` int(11) DEFAULT NULL COMMENT '1 = on 0 = off',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Set_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`Set_ID`, `Set_Title`, `Set_Logo`, `Set_About`, `Set_EmailAdmin`, `Set_Description`, `Set_Keywords`, `Set_Robots`, `Set_StatusWeb`, `created_at`, `updated_at`) VALUES
(1, 'Fameline E-library', '170720_111736.png', NULL, 'support@fameline.com', '', '', NULL, 1, '2019-08-11 00:00:00', '2020-08-26 16:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `Sli_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sli_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Sli_Img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Sli_Status` int(11) NOT NULL COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Sli_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`Sli_ID`, `Sli_Name`, `Sli_Img`, `Sli_Status`, `created_at`, `updated_at`) VALUES
(1, 'slide1', '170720_114138.jpg', 1, '2020-07-17 11:41:38', '2020-07-17 11:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories1`
--

DROP TABLE IF EXISTS `sub_categories1`;
CREATE TABLE IF NOT EXISTS `sub_categories1` (
  `Suc1_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cat_ID` int(11) NOT NULL DEFAULT 0,
  `Suc1_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Suc1_Submenu` int(11) NOT NULL DEFAULT 0 COMMENT '0 = no sub menu 1 = have sub menu',
  `Suc1_Status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Suc1_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories1`
--

INSERT INTO `sub_categories1` (`Suc1_ID`, `Cat_ID`, `Suc1_Name`, `Suc1_Submenu`, `Suc1_Status`, `created_at`, `updated_at`) VALUES
(1, 1, 'sub cate level1', 1, 1, '2020-09-02 16:13:43', '2020-09-02 16:31:27');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories2`
--

DROP TABLE IF EXISTS `sub_categories2`;
CREATE TABLE IF NOT EXISTS `sub_categories2` (
  `Suc2_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Suc1_ID` int(11) NOT NULL DEFAULT 0,
  `Suc2_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Suc2_Status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Suc2_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories2`
--

INSERT INTO `sub_categories2` (`Suc2_ID`, `Suc1_ID`, `Suc2_Name`, `Suc2_Status`, `created_at`, `updated_at`) VALUES
(1, 1, 'sub cate level2', 1, '2020-09-02 16:26:40', '2020-09-02 16:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Use_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Use_Username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Department` int(11) DEFAULT 0,
  `Use_Section` int(11) NOT NULL DEFAULT 0 COMMENT 'only knowledge innovation',
  `Use_Permission` int(11) NOT NULL DEFAULT 1 COMMENT '0 = ยกเลิก 1 = ใช้งาน',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Use_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Use_ID`, `Use_Username`, `Use_Password`, `Use_Fullname`, `Use_Email`, `Use_Tel`, `Use_Department`, `Use_Section`, `Use_Permission`, `created_at`, `updated_at`) VALUES
(1, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'test', 'abc@g.com', '1234567890', 1, 1, 1, '2020-06-09 11:33:04', '2020-07-30 16:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `visit_web`
--

DROP TABLE IF EXISTS `visit_web`;
CREATE TABLE IF NOT EXISTS `visit_web` (
  `Viw_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Viw_IP` text COLLATE utf8_unicode_ci NOT NULL,
  `Viw_Browser` text COLLATE utf8_unicode_ci NOT NULL,
  `Viw_Device` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Viw_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit_web`
--

INSERT INTO `visit_web` (`Viw_ID`, `Viw_IP`, `Viw_Browser`, `Viw_Device`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 10:16:11', '2020-07-20 10:16:11'),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 11:32:19', '2020-07-20 11:32:19'),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 11:34:06', '2020-07-20 11:34:06'),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 11:57:55', '2020-07-20 11:57:55'),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 11:58:17', '2020-07-20 11:58:17'),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:05:47', '2020-07-20 12:05:47'),
(7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:09:13', '2020-07-20 12:09:13'),
(8, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:17:05', '2020-07-20 12:17:05'),
(9, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:18:50', '2020-07-20 12:18:50'),
(10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:19:13', '2020-07-20 12:19:13'),
(11, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:19:26', '2020-07-20 12:19:26'),
(12, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:19:35', '2020-07-20 12:19:35'),
(13, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 12:47:39', '2020-07-20 12:47:39'),
(14, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 13:04:22', '2020-07-20 13:04:22'),
(15, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 14:25:31', '2020-07-20 14:25:31'),
(16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 14:26:07', '2020-07-20 14:26:07'),
(17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-20 15:44:45', '2020-07-20 15:44:45'),
(18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-22 11:54:22', '2020-07-22 11:54:22'),
(19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-24 11:01:04', '2020-07-24 11:01:04'),
(20, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Desktop', '2020-07-24 14:34:05', '2020-07-24 14:34:05'),
(21, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-07-31 17:54:39', '2020-07-31 17:54:39'),
(22, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 15:58:42', '2020-08-03 15:58:42'),
(23, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:00:37', '2020-08-03 16:00:37'),
(24, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:01:19', '2020-08-03 16:01:19'),
(25, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:06:28', '2020-08-03 16:06:28'),
(26, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:06:47', '2020-08-03 16:06:47'),
(27, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:07:17', '2020-08-03 16:07:17'),
(28, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:08:19', '2020-08-03 16:08:19'),
(29, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:08:35', '2020-08-03 16:08:35'),
(30, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:11:50', '2020-08-03 16:11:50'),
(31, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:12:06', '2020-08-03 16:12:06'),
(32, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:12:11', '2020-08-03 16:12:11'),
(33, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:16:01', '2020-08-03 16:16:01'),
(34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:24:27', '2020-08-03 16:24:27'),
(35, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:24:51', '2020-08-03 16:24:51'),
(36, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:26:48', '2020-08-03 16:26:48'),
(37, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:28:34', '2020-08-03 16:28:34'),
(38, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:29:37', '2020-08-03 16:29:37'),
(39, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:30:32', '2020-08-03 16:30:32'),
(40, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:30:54', '2020-08-03 16:30:54'),
(41, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:31:09', '2020-08-03 16:31:09'),
(42, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:31:26', '2020-08-03 16:31:26'),
(43, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:32:03', '2020-08-03 16:32:03'),
(44, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:33:10', '2020-08-03 16:33:10'),
(45, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:33:22', '2020-08-03 16:33:22'),
(46, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:33:52', '2020-08-03 16:33:52'),
(47, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:34:06', '2020-08-03 16:34:06'),
(48, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:37:29', '2020-08-03 16:37:29'),
(49, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:38:37', '2020-08-03 16:38:37'),
(50, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:39:08', '2020-08-03 16:39:08'),
(51, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:42:14', '2020-08-03 16:42:14'),
(52, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:42:52', '2020-08-03 16:42:52'),
(53, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:46:35', '2020-08-03 16:46:35'),
(54, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:47:43', '2020-08-03 16:47:43'),
(55, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:49:02', '2020-08-03 16:49:02'),
(56, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:49:43', '2020-08-03 16:49:43'),
(57, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:50:24', '2020-08-03 16:50:24'),
(58, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:50:50', '2020-08-03 16:50:50'),
(59, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:55:10', '2020-08-03 16:55:10'),
(60, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:55:29', '2020-08-03 16:55:29'),
(61, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 16:57:06', '2020-08-03 16:57:06'),
(62, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:00:23', '2020-08-03 17:00:23'),
(63, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:00:30', '2020-08-03 17:00:30'),
(64, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:00:44', '2020-08-03 17:00:44'),
(65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:00:53', '2020-08-03 17:00:53'),
(66, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:01:08', '2020-08-03 17:01:08'),
(67, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:01:36', '2020-08-03 17:01:36'),
(68, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:01:50', '2020-08-03 17:01:50'),
(69, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:08:24', '2020-08-03 17:08:24'),
(70, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:14:20', '2020-08-03 17:14:20'),
(71, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:26:16', '2020-08-03 17:26:16'),
(72, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:28:42', '2020-08-03 17:28:42'),
(73, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:29:01', '2020-08-03 17:29:01'),
(74, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:30:02', '2020-08-03 17:30:02'),
(75, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:30:51', '2020-08-03 17:30:51'),
(76, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:33:33', '2020-08-03 17:33:33'),
(77, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:33:53', '2020-08-03 17:33:53'),
(78, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:34:03', '2020-08-03 17:34:03'),
(79, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:37:40', '2020-08-03 17:37:40'),
(80, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-03 17:41:22', '2020-08-03 17:41:22'),
(81, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:48:23', '2020-08-04 12:48:23'),
(82, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:49:30', '2020-08-04 12:49:30'),
(83, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:50:38', '2020-08-04 12:50:38'),
(84, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:51:12', '2020-08-04 12:51:12'),
(85, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:51:46', '2020-08-04 12:51:46'),
(86, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:54:09', '2020-08-04 12:54:09'),
(87, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:54:23', '2020-08-04 12:54:23'),
(88, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:54:43', '2020-08-04 12:54:43'),
(89, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:54:57', '2020-08-04 12:54:57'),
(90, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:55:34', '2020-08-04 12:55:34'),
(91, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:57:26', '2020-08-04 12:57:26'),
(92, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 12:59:56', '2020-08-04 12:59:56'),
(93, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 13:02:25', '2020-08-04 13:02:25'),
(94, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:45:26', '2020-08-04 15:45:26'),
(95, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:47:59', '2020-08-04 15:47:59'),
(96, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:48:47', '2020-08-04 15:48:47'),
(97, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:53:11', '2020-08-04 15:53:11'),
(98, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:54:17', '2020-08-04 15:54:17'),
(99, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 15:59:40', '2020-08-04 15:59:40'),
(100, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:07:20', '2020-08-04 16:07:20'),
(101, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:08:16', '2020-08-04 16:08:16'),
(102, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:09:40', '2020-08-04 16:09:40'),
(103, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:11:18', '2020-08-04 16:11:18'),
(104, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:11:32', '2020-08-04 16:11:32'),
(105, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:13:15', '2020-08-04 16:13:15'),
(106, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:14:11', '2020-08-04 16:14:11'),
(107, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:14:45', '2020-08-04 16:14:45'),
(108, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:17:31', '2020-08-04 16:17:31'),
(109, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:18:25', '2020-08-04 16:18:25'),
(110, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:19:06', '2020-08-04 16:19:06'),
(111, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:20:02', '2020-08-04 16:20:02'),
(112, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:21:10', '2020-08-04 16:21:10'),
(113, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:23:25', '2020-08-04 16:23:25'),
(114, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:26:25', '2020-08-04 16:26:25'),
(115, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:27:15', '2020-08-04 16:27:15'),
(116, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:27:53', '2020-08-04 16:27:53'),
(117, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:28:09', '2020-08-04 16:28:09'),
(118, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 16:33:40', '2020-08-04 16:33:40'),
(119, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 18:06:16', '2020-08-04 18:06:16'),
(120, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 19:19:34', '2020-08-04 19:19:34'),
(121, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 19:23:42', '2020-08-04 19:23:42'),
(122, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-04 19:24:21', '2020-08-04 19:24:21'),
(123, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-05 11:34:58', '2020-08-05 11:34:58'),
(124, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-05 11:45:34', '2020-08-05 11:45:34'),
(125, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-05 17:17:54', '2020-08-05 17:17:54'),
(126, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 12:59:47', '2020-08-07 12:59:47'),
(127, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 13:43:39', '2020-08-07 13:43:39'),
(128, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 13:49:52', '2020-08-07 13:49:52'),
(129, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 14:14:30', '2020-08-07 14:14:30'),
(130, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 14:32:36', '2020-08-07 14:32:36'),
(131, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-07 14:34:55', '2020-08-07 14:34:55'),
(132, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-10 14:20:30', '2020-08-10 14:20:30'),
(133, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-10 18:00:02', '2020-08-10 18:00:02'),
(134, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-10 18:02:03', '2020-08-10 18:02:03'),
(135, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-11 10:58:49', '2020-08-11 10:58:49'),
(136, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:24:54', '2020-08-21 11:24:54'),
(137, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:34:14', '2020-08-21 11:34:14'),
(138, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:34:28', '2020-08-21 11:34:28'),
(139, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:34:44', '2020-08-21 11:34:44'),
(140, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:35:33', '2020-08-21 11:35:33'),
(141, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 11:35:40', '2020-08-21 11:35:40'),
(142, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:08:58', '2020-08-21 12:08:58'),
(143, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:03', '2020-08-21 12:11:03'),
(144, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:12', '2020-08-21 12:11:12'),
(145, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:13', '2020-08-21 12:11:13'),
(146, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:13', '2020-08-21 12:11:13'),
(147, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:13', '2020-08-21 12:11:13'),
(148, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:13', '2020-08-21 12:11:13'),
(149, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:11:13', '2020-08-21 12:11:13'),
(150, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:36', '2020-08-21 12:12:36'),
(151, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:36', '2020-08-21 12:12:36'),
(152, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:36', '2020-08-21 12:12:36'),
(153, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:36', '2020-08-21 12:12:36'),
(154, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:36', '2020-08-21 12:12:36'),
(155, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:12:37', '2020-08-21 12:12:37'),
(156, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:02', '2020-08-21 12:18:02'),
(157, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:02', '2020-08-21 12:18:02'),
(158, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:02', '2020-08-21 12:18:02'),
(159, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:03', '2020-08-21 12:18:03'),
(160, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:03', '2020-08-21 12:18:03'),
(161, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:03', '2020-08-21 12:18:03'),
(162, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:03', '2020-08-21 12:18:03'),
(163, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:03', '2020-08-21 12:18:03'),
(164, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:45', '2020-08-21 12:18:45'),
(165, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:45', '2020-08-21 12:18:45'),
(166, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:45', '2020-08-21 12:18:45'),
(167, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:46', '2020-08-21 12:18:46'),
(168, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:18:46', '2020-08-21 12:18:46'),
(169, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:18', '2020-08-21 12:19:18'),
(170, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:18', '2020-08-21 12:19:18'),
(171, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:19', '2020-08-21 12:19:19'),
(172, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:19', '2020-08-21 12:19:19'),
(173, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:19', '2020-08-21 12:19:19'),
(174, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:19', '2020-08-21 12:19:19'),
(175, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-21 12:19:19', '2020-08-21 12:19:19'),
(176, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-25 15:05:31', '2020-08-25 15:05:31'),
(177, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 10:20:20', '2020-08-26 10:20:20'),
(178, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 10:24:57', '2020-08-26 10:24:57'),
(179, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 11:01:57', '2020-08-26 11:01:57'),
(180, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 11:02:40', '2020-08-26 11:02:40'),
(181, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:20:12', '2020-08-26 12:20:12'),
(182, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:56', '2020-08-26 12:21:56'),
(183, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:56', '2020-08-26 12:21:56'),
(184, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:56', '2020-08-26 12:21:56'),
(185, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:56', '2020-08-26 12:21:56'),
(186, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:57', '2020-08-26 12:21:57'),
(187, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:57', '2020-08-26 12:21:57'),
(188, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:57', '2020-08-26 12:21:57'),
(189, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:57', '2020-08-26 12:21:57'),
(190, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 12:21:57', '2020-08-26 12:21:57'),
(191, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 13:30:29', '2020-08-26 13:30:29'),
(192, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 13:38:33', '2020-08-26 13:38:33'),
(193, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 14:46:50', '2020-08-26 14:46:50'),
(194, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:05:46', '2020-08-26 16:05:46'),
(195, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:05:51', '2020-08-26 16:05:51'),
(196, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:29:01', '2020-08-26 16:29:01'),
(197, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:31:43', '2020-08-26 16:31:43'),
(198, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:35:00', '2020-08-26 16:35:00'),
(199, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:40:55', '2020-08-26 16:40:55'),
(200, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:41:51', '2020-08-26 16:41:51'),
(201, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:42:12', '2020-08-26 16:42:12'),
(202, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-26 16:50:27', '2020-08-26 16:50:27'),
(203, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-28 11:16:02', '2020-08-28 11:16:02'),
(204, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'Desktop', '2020-08-28 11:42:47', '2020-08-28 11:42:47'),
(205, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-01 17:06:52', '2020-09-01 17:06:52'),
(206, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-02 16:47:02', '2020-09-02 16:47:02'),
(207, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-03 15:33:42', '2020-09-03 15:33:42'),
(208, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-03 15:48:03', '2020-09-03 15:48:03'),
(209, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-08 16:40:47', '2020-09-08 16:40:47'),
(210, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-09 11:14:55', '2020-09-09 11:14:55'),
(211, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-10 11:10:31', '2020-09-10 11:10:31'),
(212, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'Desktop', '2020-09-18 12:46:18', '2020-09-18 12:46:18');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
