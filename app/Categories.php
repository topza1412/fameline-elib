<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categories extends Model {
 
     protected $table = 'categories';
 
     protected $primaryKey = 'Cat_ID';

     public $timestamps = true;

}

