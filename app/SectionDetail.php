<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SectionDetail extends Model {
 
     protected $table = 'section_detail';
 
     protected $primaryKey = 'Sed_ID';

     public $timestamps = true;

}

