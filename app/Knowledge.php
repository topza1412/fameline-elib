<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Knowledge extends Model {
 
     protected $table = 'knowledge';
 
     protected $primaryKey = 'Kno_ID';

     public $timestamps = true;

 
}

