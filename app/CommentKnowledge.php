<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CommentKnowledge extends Model {
 
     protected $table = 'comment_knowledge';
 
     protected $primaryKey = 'Com_ID';

     public $timestamps = true;

}

