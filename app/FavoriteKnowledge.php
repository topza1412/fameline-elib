<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FavoriteKnowledge extends Model {
 
     protected $table = 'favorite_knowledge';
 
     protected $primaryKey = 'Fav_ID';

     public $timestamps = true;

}

