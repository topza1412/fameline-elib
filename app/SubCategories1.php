<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubCategories1 extends Model {
 
     protected $table = 'sub_categories1';
 
     protected $primaryKey = 'Suc1_ID';

     public $timestamps = true;

}

