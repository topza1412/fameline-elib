<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubCategories2 extends Model {
 
     protected $table = 'sub_categories2';
 
     protected $primaryKey = 'Suc2_ID';

     public $timestamps = true;

}

