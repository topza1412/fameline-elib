<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
 
     protected $table = 'contact';
 
     protected $primaryKey = 'Con_ID';

     public $timestamps = true;

}

