<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Section extends Model {
 
     protected $table = 'section';
 
     protected $primaryKey = 'Sec_ID';

     public $timestamps = true;

}

