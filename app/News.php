<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class News extends Model {
 
     protected $table = 'news';
 
     protected $primaryKey = 'New_ID';

     public $timestamps = true;

}

