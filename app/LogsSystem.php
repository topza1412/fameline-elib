<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class LogsSystem extends Model {
 
     protected $table = 'logs_system';
 
     protected $primaryKey = 'Log_ID';

     public $timestamps = true;

}

