<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class LogsKnowledge extends Model {
 
     protected $table = 'logs_knowledge';
 
     protected $primaryKey = 'Lok_ID';

     public $timestamps = true;

}

