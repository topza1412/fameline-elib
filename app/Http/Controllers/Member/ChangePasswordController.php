<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\User;

class ChangePasswordController extends Controller
{

    public function index () {

        //seo
        $seo = $this->Seo(null,'Change Password','Change Password','Change Password','Change Password',url('changepassword'));

        $data = ['seo' => $seo];

        return view('page.member.changepassword',['data' => $data]);
    }

    public function verify (Request $request) {

        if($request->post()){

            $this->validate(request(), [
                'password' => 'required|string|min:6|confirmed',
            ]);

            $input = $request->all();

            $result = User::where(['Use_ID' => session('session_id')])->get();

            if($result==true){
                    $result = User::where(['Use_ID' => session('session_id')])->update(['Use_Password' => md5($input['password']),'updated_at' => now()]);
                if($result==true){
                    return back()->with('success',trans('login.forgotpassword_newpassword'));    
                } else{
                    return back()->with('error',trans('login.forgotpassword_failed'));   
                }
            } else{
                return back()->with('error',trans('login.forgotpassword_failed'));    
            }

         }

    }

}
