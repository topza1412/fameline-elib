<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\SendMail;
use App\User;

class ForgotpasswordController extends Controller
{

    public function __construct()
    {
       
    }

    public function index()
    {   
       //seo
       $seo = $this->Seo(null,'Forgot password','Forgot password','Forgot password','Forgot password',url('forgotpassword'));

       $data = ['step' => 'step1','token_email' => null,'seo' => $seo];

       return view('page.member.forgotpassword',['data' => $data]);  
    }


    public function changepassword($token_email)
    {   
        //seo
        $seo = $this->Seo(null,'Forgot password','Forgot password','Forgot password','Forgot password',url('forgotpassword'));

        $data = ['step' => 'step2','token_email' => $token_email,'seo' => $seo];

        return view('page.member.forgotpassword',['data' => $data]);  
    }

    public function sendmail(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
            ]);

            $input = $request->all();

            $checkusername = User::where('Use_Username',$input['username'])->get();
            $checkemail = User::where('Use_Email',$input['email'])->get();

            if(count($checkusername)==0){
                return back()->with('error',trans('login.forgotpassword_username_failed'));
                exit;
            }

            if(count($checkemail)==0){
                return back()->with('error',trans('login.forgotpassword_email_failed'));
                exit;
            }

            if(count($checkusername)>0 && count($checkemail)>0){

                //send mail 
                $token_email = base64_encode($input['email']);

                $data = array(
                    'email_from' => $this->SettingWeb()->Set_EmailAdmin,
                    'subject' => 'Forgot Password'.' ('.$this->SettingWeb()->Set_Title.')',
                    'page' => 'page.member.mail.forgotpassword',
                    'input' =>  $request->all(),
                    'forgotpassword_link' => url('forgotpassword/changepassword/'.$token_email)
                );

                Mail::to($input['email'])->send(new SendMail($data));

                if (Mail::failures()) {
                    return back()->with('error',trans('login.forgotpassword_failed'));   
                }else{
                    return back()->with('success',trans('login.forgotpassword_success')); 
                }


            }

        }
    }


    public function verify(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'password' => 'required|string|min:6|confirmed',
            ]);

            $input = $request->all();

            $token_email = base64_decode($input['token_email']);

            $result = User::where(['Use_Email' => $token_email])->get();

            if($result==true){
            $result = User::where(['Use_Email' => $token_email])->update(['Use_Password' => md5($input['password']));
                if($result==true){
                    return back()->with('success',trans('login.forgotpassword_newpassword'));    
                } else{
                    return back()->with('error',trans('login.forgotpassword_failed'));   
                }
            } else{
                return back()->with('error',trans('login.forgotpassword_failed'));    
            }

        }

    }

}
