<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Knowledge;

class DashboardController extends Controller
{
    public function index() {

      $result = Knowledge::select('knowledge.created_at as date_create', 'knowledge.*', 'admin.*', 'favorite_knowledge.*')
      ->join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
      ->join('favorite_knowledge', 'favorite_knowledge.Kno_ID', '=', 'knowledge.Kno_ID')
      ->join('user', 'user.Use_ID', '=', 'favorite_knowledge.Use_ID')
      ->where('favorite_knowledge.Use_ID', session('session_id'))
      ->orderby('knowledge.Kno_ID','desc')
      ->paginate(6);

      //seo
      $seo = $this->Seo(null,'Dashboard','Dashboard','Dashboard','Dashboard',url('dashboard'));

      $data = array('result' => $result, 'seo' => $seo);

      return view('page.member.dashboard',['data' => $data]);
    }

}
