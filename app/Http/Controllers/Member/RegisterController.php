<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\SendMail;
use App\User;

class RegisterController extends Controller
{


    public function register(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'fullname' => 'required|string|max:255',
                'use_username' => 'required|string|unique:user|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'password' => 'required|string|min:6',
                'use_email' => 'required|string|unique:user|email|max:255',
            ]);

            $result = new User;
            $result->Use_Fullname = $request->fullname;
            $result->Use_Username = $request->use_username;
            $result->Use_Password = md5($request->password);
            $result->Use_Email = $request->use_email;
            $result->Use_Permission = true;
            $result->save();


            if($result){

                //send mail register
                $data = array(
                    'email_from' => $this->SettingWeb()->Set_EmailAdmin,
                    'subject' => 'New Register'.' ('.$this->SettingWeb()->Set_Title.')',
                    'page' => 'page.member.mail.register',
                    'input' =>  $request->all(),
                );

                Mail::to($input['email'])->send(new SendMail($data));

                return back()->with('success',trans('register.register_success')); 

            } else {
                return back()->with('error',trans('register.register_failed'));
            }

        }
    }

}
