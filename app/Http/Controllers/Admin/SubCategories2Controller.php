<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\SubCategories1;
use App\SubCategories2;

class SubCategories2Controller extends Controller
{ 
    public function __construct()
    {
        
    }

    public function page($page,Request $request){

      if($page=='core'){
        $result = SubCategories2::select('sub_categories2.created_at as date_create', 'categories.*', 'sub_categories2.*', 'sub_categories1.*')
        ->join('sub_categories1', 'sub_categories1.Suc1_ID', '=', 'sub_categories2.Suc1_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',1)
        ->orderby('sub_categories2.Suc2_ID','desc')
        ->get();
  	  } else if($page=='advance'){
    	  $result = SubCategories2::select('sub_categories2.created_at as date_create', 'categories.*', 'sub_categories2.*', 'sub_categories1.*')
        ->join('sub_categories1', 'sub_categories1.Suc1_ID', '=', 'sub_categories2.Suc1_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',2)
        ->orderby('sub_categories2.Suc2_ID','desc')
        ->get();
  	  } else if($page=='innovation'){
        $result = SubCategories2::select('sub_categories2.created_at as date_create', 'categories.*', 'sub_categories2.*', 'sub_categories1.*')
        ->join('sub_categories1', 'sub_categories1.Suc1_ID', '=', 'sub_categories2.Suc1_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',3)
        ->orderby('sub_categories2.Suc2_ID','desc')
        ->get();
      }

      $data = ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.subcategories2',['data' => $data]);
    }


    public function form_add ($page) {

      if($page=='core'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 1, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      } else if($page=='advance'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 2, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      } else if($page=='innovation'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 3, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      }
      
      $data = ['page' => $page,'main_cate' => $main_cate,'type' => 'form','action' => 'insert'];

      return view('page.admin.subcategories2',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      if($page=='core'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 1, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      } else if($page=='advance'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 2, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      } else if($page=='innovation'){
        $main_cate = SubCategories1::join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where(['categories.Gro_ID' => 3, 'sub_categories1.Suc1_Status' => 1])
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      }

      $result = SubCategories2::where('Suc2_ID',$id)->first();

      $data = ['page' => $page,'main_cate' => $main_cate,'result' => $result,'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.subcategories2',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

          $this->validate(request(), [
          'sub_cate1' => 'required',
          'sub_cate2' => 'required|string|max:255',
          ]);


          if($request->type_action=='insert'){

            $result = new SubCategories2;
            $result->Suc1_ID = $request->sub_cate1;
            $result->Suc2_Name = $request->sub_cate2;
            $result->Suc2_Status = 1;
            $result->save();

            SubCategories1::where('Suc1_ID',$request->sub_cate1)->update(['Suc1_Submenu' => 1]);

            $this->LogsSystem('sub categories level 2', 'insert', $request->sub_cate2);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';
            
          } else if($request->type_action=='update'){

            	$data = [
            	'Suc1_ID' => $request->sub_cate1,
            	'Suc2_Name' => $request->sub_cate2,
              'Suc2_Status' => $request->status,
              'updated_at' => now(),
            	];
            	$result = SubCategories2::where('Suc2_ID',$request->id)->update($data);

              $this->LogsSystem('sub categories level 2', 'insert', $request->sub_cate2);

              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

  }

}

    public function delete ($page,$id) {

    	$result = Categories::where('Cat_ID',$id)->delete();

    	if($result){
         $this->LogsSystem('sub categories level 2', 'delete', 'sub categories level 2 id '.$id);
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
