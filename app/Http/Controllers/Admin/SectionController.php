<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Knowledge;

class SectionController extends Controller
{ 
    public function __construct()
    {
        
    }

    public function page($page,Request $request){

      if($page == 'group'){
        $result = Section::orderby('Sec_ID','desc')->get();
      } else if($page == 'detail'){
        $result = Section::join('knowledge', 'knowledge.Sec_ID', '=', 'section.Sec_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
        ->where('section.Sec_ID', $request->id)
        ->orderby('knowledge.Kno_ID','desc')
        ->get();
      }

      $data = ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.section',['data' => $data]);
    }


    public function form_add ($page) {
      
      $data = ['page' => $page,'result' => null,'type' => 'form','action' => 'insert'];

      return view('page.admin.section',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      $result = Section::where('Sec_ID',$id)->first();

      $data = ['page' => $page,'result' => $result,'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.section',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

          if($request->type_action=='insert'){

                $this->validate(request(), [
                'sec_name' => 'required|string|unique:section|max:255',
                'group' => 'required',
                ]);

            $result = new Section;
            $result->Sec_Name = $request->sec_name;
            $result->Gro_ID = $request->group;
            $result->Sec_Status = 1;
            $result->save();

            $this->LogsSystem('section', 'insert', $request->sec_name);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';

          } else if($request->type_action=='update'){

              $this->validate(request(), [
                    'sec_name' => 'required|string|max:255',
                    'group' => 'required',
                    ]);

            	$data = [
            	'Sec_Name' => $request->sec_name,
            	'Gro_ID' => $request->group,
              'Sec_Status' => $request->status,
              'updated_at' => now(),
            	];
            	$result = Section::where('Sec_ID',$request->id)->update($data);

              $this->LogsSystem('section', 'update', $request->sec_name);
              
              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

    	if($result){
    	   return back()->with('success',trans('other.'.$alert_success));
    	} else{
    	   return back()->with('error',trans('other.'.$alert_not_success));
    	}

  }

}

    public function delete ($page,$id) {

      if($page == 'group'){
        $result = Section::where('Sec_ID',$id)->delete();
        $this->LogsSystem('section', 'delete', 'section id '.$id);
      } else{
        $result = SectionDetail::where('Sed_ID',$id)->delete();
        $this->LogsSystem('section', 'delete', 'section detail id '.$id);
      }

    	if($result){
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
