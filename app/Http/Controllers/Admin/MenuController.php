<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Home;
use App\About;
use App\Contact;

class MenuController extends Controller
{

    public function __construct()
    {
        $this->middleware('session_login_admin');
    }
    
    public function page($page){

      if($page=='home'){
        $detail = Home::all();
  	  } else if($page=='about'){
        $detail = About::all();
      } else if($page=='contact'){
    	  $detail = Contact::all();
  	  }

      $data = ['page' => $page,'detail' => $detail];

      return view('page.admin.menu',['data' => $data]);

}



    public function action ($page,Request $request) {

      if($request->post()){

      	if($page=='home'){
          if($request->img==null){
                $file_upload = $request->img_hidden;
          } else{
                $file_upload = $this->Upload_File($request->file('img'), 'upload/admin/home/', 700, 450);
          }
          $data = [
          'Hom_Detail' => ($request->detail != null) ? $request->detail : null,
          'Hom_Img' => $file_upload
          ];
          $result = DB::table('home_detail')->update($data);

          $this->LogsSystem('home menu', 'update', 'home menu');

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';
        } else if($page=='about'){
        	$data = [
        	'Abo_Detail' => ($request->detail != null) ? $request->detail : null
        	];
        	$result = DB::table('about')->update($data);

          $this->LogsSystem('about menu', 'update', 'about menu');

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';
      	} else if($page=='contact'){
          $data = [
          'Con_Detail' => ($request->detail != null) ? $request->detail : null
          ];
          
          $result = DB::table('contact')->update($data);

          $this->LogsSystem('contact menu', 'update', 'contact menu');

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';
        }


      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

       }


}


}
