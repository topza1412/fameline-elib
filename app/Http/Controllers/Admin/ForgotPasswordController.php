<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\SendMail;
use App\Admin;

class ForgotpasswordController extends Controller
{

    public function __construct()
    {
       
    }

    public function index()
    {   

       $data = ['step' => 'step1','token_email' => null];
       return view('page.admin.forgotpassword',['data' => $data]);  
    }


    public function changepassword($token_email)
    {   

       $data = ['step' => 'step2','token_email' => $token_email];
       return view('page.admin.forgotpassword',['data' => $data]);  
    }

    public function sendmail(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
            ]);

            $input = $request->all();

            $checkusername = Admin::where('Adm_Username',$input['username'])->get();
            $checkemail = Admin::where('Adm_Email',$input['email'])->get();

            if(count($checkusername)==0){
                return back()->with('error',trans('login.forgotpassword_username_failed'));
            }

            if(count($checkemail)==0){
                return back()->with('error',trans('login.forgotpassword_email_failed'));
            }

            if(count($checkusername)>0 && count($checkemail)>0){

                $token_email = base64_encode($input['email']);

                $data = array(
                    'email_from' => $this->SettingWeb()->Set_EmailAdmin,
                    'subject' => 'Forgot Password Admin'.' ('.$this->SettingWeb()->Set_Title.')',
                    'page' => 'page.member.mail.forgotpassword',
                    'input' =>  $request->all(),
                    'forgotpassword_link' => url('admin/forgotpassword/changepassword/'.$token_email)
                );

                Mail::to($input['email'])->send(new SendMail($data));

                if (Mail::failures()) {
                    $this->LogsSystem('send mail success', 'sendmail', 'forgot password username '.$input['username']);
                    return back()->with('error',trans('login.forgotpassword_failed'));   
                } else{
                    $this->LogsSystem('send mail failed', 'sendmail', 'forgot password username '.$input['username']);
                    return back()->with('success',trans('login.forgotpassword_success')); 
                }

            }

        }

    }


    public function verify(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'password' => 'required|string|min:6|confirmed',
            ]);

            $input = $request->all();

            $token_email = base64_decode($input['token_email']);

            $result = Admin::where(['Adm_Email' => $token_email])->get();

            if($result==true){
                $result = Admin::where(['Adm_Email' => $token_email])->update(['Adm_Password' => md5($input['password']),'updated_at' => now()]);
                if($result==true){
                    $this->LogsSystem('forgot password success', 'forgotpassword', 'username '.$input['username']);
                    return back()->with('success',trans('login.forgotpassword_newpassword'));    
                } else{
                    $this->LogsSystem('forgot password failed', 'forgotpassword', 'username '.$input['username']);
                    return back()->with('error',trans('login.forgotpassword_failed'));   
                }
            } else{
                return back()->with('error',trans('login.forgotpassword_failed'));    
            }

        }

    }

}
