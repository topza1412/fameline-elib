<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Admin;
use App\RoleAdmin;

class LoginController extends Controller
{


    public function index () {

        return view('page.admin.login');
    }


    public function auth(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'username' => 'required|string|max:255',
                'password' => 'required|string|min:6',
            ]);

            $result = Admin::where(['Adm_Username' => $request->username, 'Adm_Password' => md5($request->password)])->first();

            if($result){
                if($result->Adm_Permission == false){
                    return back()->with('error',trans('login.permission_failed'));
                } else {
                    if($request->remember_login != null){
                        setcookie ("remember_login",$request->remember_login,time()+ (10 * 365 * 24 * 60 * 60));
                        setcookie ("remember_username",$request->username,time()+ (10 * 365 * 24 * 60 * 60));
                        setcookie ("remember_password",$request->password,time()+ (10 * 365 * 24 * 60 * 60));
                    } else{
                        setcookie ("remember_login",null);
                        setcookie ("remember_username",null);
                        setcookie ("remember_password",null);    
                    }
                        $data = Admin::find($result->Adm_ID);
                        $data_role = RoleAdmin::where('Adm_ID', $data->Adm_ID)->first();
                        session()->put('session_admin_id',$data->Adm_ID);
                        session()->put('session_admin_username',$data->Adm_Username);
                        session()->put('session_admin_department',$data->Adm_Department);
                        session()->put('session_admin_status',$data->Adm_Status);
                        session()->put('session_admin_role',$data_role);
                        Session::flash('login_success', true);

                        $this->LogsSystem('login success', 'login', $request->username);

                        return redirect('admin/home');
                }

            } else{

                $this->LogsSystem('login failed', 'login', $request->username);
                
                return back()->with('error',trans('login.login_failed'));   
            }

        }


    }

    public function logout(){

        $this->LogsSystem('logout', 'login', session('session_admin_username'));

        Session::flush();

        return redirect('admin/login');
    }


}
