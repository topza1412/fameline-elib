<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Admin;

class ProfileController extends Controller
{


  public function __construct()
    {
        
    }

    public function page ($page,Request $request) {

      if($page == 'profile'){
        $result = Admin::where('Adm_ID',session('session_admin_id'))->first();
      } else{
        $result = null;
      }

      $data = ['page' => $page,'result' => $result,'action' => 'update','id' => session('session_admin_id')];
      return view('page.admin.profile',['data' => $data]);  

    }


    public function action ($page,Request $request) {

    if($request->post()){

      if($page == 'profile'){

          $this->validate(request(), [
              'name' => 'required|string|max:255',
              'username' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
              'email' => 'required|string|email|max:255',
              'tel' => 'required|min:10|numeric',
              ]);

          $username = Admin::where('Adm_Username',$request->username)->where('Adm_ID','!=',session('session_admin_id'))->first();
          $email = Admin::where('Adm_Email',$request->email)->where('Adm_ID','!=',session('session_admin_id'))->first();

          if($username != null){
              return trans('profile.profile_username_duplicate');
          }

          if($email != null){
              return trans('profile.profile_email_duplicate');
          }

          if($username == null && $email == null){

          	$data = [
          	'Adm_Fullname' => $request->name,
          	'Adm_Username' => $request->username,
          	'Adm_Email' => $request->email,
          	'Adm_Tel' => $request->tel,
            'updated_at' => now(),
          	];
          	$result = Admin::where('Adm_ID',$request->id)->update($data);

            $this->LogsSystem('profile update', 'profile', 'username '.$request->username);

            $alert_success = 'edit_success';
            $alert_not_success = 'edit_not_success';

          }

      } else{
        
          $this->validate(request(), [
                  'old_password' => 'required|string|min:6',
                  'password' => 'required|string|min:6|confirmed',
              ]);

          $checkPassword = Admin::where('Adm_Password',md5($request->old_password))->where('Adm_ID', session('session_admin_id'))->first();

          if($checkPassword == null){
              return trans('profile.profile_password_duplicate');
          }

          $result = Admin::where(['Adm_ID' => session('session_admin_id')])->update(['Adm_Password' => md5($request->password),'updated_at' => now()]);

          $this->LogsSystem('change password', 'profile', 'admin id '.session('session_admin_id'));

          $alert_success = 'forgotpassword_newpassword';
          $alert_not_success = 'forgotpassword_failed';
      }

    }

    	if($result){
    	   return back()->with('success',trans('other.'.$alert_success));
    	} else{
    	   return back()->with('error',trans('other.'.$alert_not_success));
    	}

  }

    
}
