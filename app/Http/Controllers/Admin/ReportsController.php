<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Knowledge;
use App\LogsKnowledge;
use App\LogsSystem;
use App\User;
use App\VisitWeb;

class ReportsController extends Controller
{ 

  public function __construct()
    {
        
    }

    public function page($page){

      $data = ['page' => $page,'type' => 'view'];

      return view('page.admin.reports',['data' => $data]);

    }


    public function search ($page,Request $request) {

      if($request->post()){

        	if($page=='logknowledge'){

            $result = LogsKnowledge::join('admin', 'admin.Adm_ID', '=', 'logs_knowledge.Adm_ID')
            ->join('knowledge', 'knowledge.Kno_ID', '=', 'logs_knowledge.Kno_ID')
            ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
            ->whereBetween('logs_knowledge.created_at', [$request->date_start, $request->date_end])
            ->orderby('logs_knowledge.Log_ID','desc')
            ->get();

        	} else if($page=='logsystem'){

            $result = LogsSystem::join('admin', 'admin.Adm_ID', '=', 'logs_system.Adm_ID')
            ->whereBetween('logs_system.created_at', [$request->date_start, $request->date_end])
            ->orderby('logs_system.Log_ID','desc')
            ->get();

          } else if($page=='member'){

            $result = LogsKnowledge::whereBetween('created_at', [$request->date_start, $request->date_end])
            ->orderby('Use_ID','desc')
            ->get();

          } else if($page=='visitweb'){

            $result = VisitWeb::whereBetween('created_at', [$request->date_start, $request->date_end])
            ->orderby('Viw_ID','desc')
            ->get();

          }

          if($result){

            $this->LogsSystem('report', 'report', $page);

            $data = ['result' => $result,'page' => $page,'type' => 'search','date_start' => $request->date_start,'date_end' => $request->date_end];

          	return view('page.admin.reports',['data' => $data]);

          } else{

            return back()->with('error','No report data!');  

          }

          }
      }

}
