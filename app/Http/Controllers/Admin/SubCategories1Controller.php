<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\SubCategories1;
use App\Categories;

class SubCategories1Controller extends Controller
{ 
    public function __construct()
    {
        
    }

    public function page($page,Request $request){

      if($page=='core'){
        $result = SubCategories1::select('sub_categories1.created_at as date_create', 'sub_categories1.*', 'categories.*')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',1)
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
  	  } else if($page=='advance'){
    	  $result = SubCategories1::select('sub_categories1.created_at as date_create', 'sub_categories1.*', 'categories.*')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',2)
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
  	  } else if($page=='innovation'){
        $result = SubCategories1::select('sub_categories1.created_at as date_create', 'sub_categories1.*', 'categories.*')
        ->join('categories', 'categories.Cat_ID', '=', 'sub_categories1.Cat_ID')
        ->where('categories.Gro_ID',3)
        ->orderby('sub_categories1.Suc1_ID','desc')
        ->get();
      }

      $data = ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.subcategories1',['data' => $data]);
    }


    public function form_add ($page) {

      if($page=='core'){
        $main_cate = Categories::where(['Gro_ID' => 1, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      } else if($page=='advance'){
        $main_cate = Categories::where(['Gro_ID' => 2, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      } else if($page=='innovation'){
        $main_cate = Categories::where(['Gro_ID' => 3, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      }
      
      $data = ['page' => $page,'main_cate' => $main_cate,'type' => 'form','action' => 'insert'];

      return view('page.admin.subcategories1',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      if($page=='core'){
        $main_cate = Categories::where(['Gro_ID' => 1, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      } else if($page=='advance'){
        $main_cate = Categories::where(['Gro_ID' => 2, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      } else if($page=='innovation'){
        $main_cate = Categories::where(['Gro_ID' => 3, 'Cat_Status' => 1])->orderby('Cat_ID','desc')->get();
      }

      $result = SubCategories1::where('Suc1_ID',$id)->first();

      $data = ['page' => $page,'main_cate' => $main_cate,'result' => $result,'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.subcategories1',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

          $this->validate(request(), [
          'cate_main' => 'required',
          'sub_cate' => 'required|string|max:255',
          ]);

          if($request->type_action=='insert'){

            $result = new SubCategories1;
            $result->Cat_ID = $request->cate_main;
            $result->Suc1_Name = $request->sub_cate;
            $result->Suc1_Status = 1;
            $result->save();

            Categories::where('Cat_ID',$request->cate_main)->update(['Cat_Submenu' => 1]);

            $this->LogsSystem('sub categories level 1', 'insert', $request->sub_cate);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';
            
          } else if($request->type_action=='update'){

            	$data = [
            	'Cat_ID' => $request->cate_main,
            	'Suc1_Name' => $request->sub_cate,
              'Suc1_Status' => $request->permission,
              'updated_at' => now(),
            	];
            	$result = SubCategories1::where('Suc1_ID',$request->id)->update($data);

              $this->LogsSystem('sub categories level 1', 'update', $request->sub_cate);

              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

  }

}

    public function delete ($page,$id) {

    	$result = SubCategories1::where('Suc1_ID',$id)->delete();

    	if($result){
         $this->LogsSystem('sub categories level 1', 'delete', 'sub categories level 1 id '.$id);
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
