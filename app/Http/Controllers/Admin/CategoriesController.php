<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Categories;

class CategoriesController extends Controller
{ 
    public function __construct()
    {
        
    }

    public function page($page,Request $request){

      if($page=='core'){
        $result = Categories::where('Gro_ID',1)->orderby('Cat_ID','desc')->get();
  	  } else if($page=='advance'){
    	  $result = Categories::where('Gro_ID',2)->orderby('Cat_ID','desc')->get();
  	  } else if($page=='innovation'){
        $result = Categories::where('Gro_ID',3)->orderby('Cat_ID','desc')->get();
      }

      $data = ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.categories',['data' => $data]);
    }


    public function form_add ($page) {
      
      $data = ['page' => $page,'result' => null,'type' => 'form','action' => 'insert'];

      return view('page.admin.categories',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      $result = Categories::where('Cat_ID',$id)->first();

      $data = ['page' => $page,'result' => $result,'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.categories',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

          if($request->type_action=='insert'){

                $this->validate(request(), [
                'name' => 'required|string|max:255',
                'group' => 'required',
                ]);

            $result = new Categories;
            $result->Cat_Name = $request->name;
            $result->Gro_ID = $request->group;
            $result->Cat_Status = 1;
            $result->save();

            $this->LogsSystem('categories', 'insert', $request->name);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';
            
          } else if($request->type_action=='update'){

              $this->validate(request(), [
                    'name' => 'required|string|max:255',
                    'group' => 'required',
                    ]);

            	$data = [
            	'Cat_Name' => $request->name,
            	'Gro_ID' => $request->group,
              'updated_at' => now(),
            	];
            	$result = Categories::where('Cat_ID',$request->id)->update($data);

              $this->LogsSystem('categories', 'update', $request->name);

              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

  }

}

    public function delete ($page,$id) {

    	$result = Categories::where('Cat_ID',$id)->delete();

    	if($result){
         $this->LogsSystem('categories', 'delete', 'categories id '.$id);
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
