<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Admin;
use App\User;
use App\RoleUser;
use App\RoleAdmin;
use App\Section;
use App\SectionDetail;

class MemberController extends Controller
{ 
    public function __construct()
    {

    }

    public function page($page,Request $request){

      if($page=='admin'){
        $detail = Admin::where('Adm_Status',0)->orderby('Adm_ID','desc')->get();
  	  } else if($page=='user'){
        if(session('session_admin_status') == 1){
    	    $detail = User::orderby('Use_ID','desc')->get();
        } else{
          $detail = User::where('Use_Department', session('session_admin_department'))->orderby('Use_ID','desc')->get();
        }
  	  } 

      $data = ['page' => $page,'detail' => $detail,'type' => 'view'];

      return view('page.admin.member',['data' => $data]);
    }


    public function form_add ($page) {

      if($page=='admin'){
        $data = ['page' => $page,'detail' => null,'type' => 'form','action' => 'insert'];
      } else if($page=='user'){
        $section = Section::where('Sec_Status',1)->get();
        $data = ['page' => $page,'detail' => null, 'section' => $section, 'type' => 'form','action' => 'insert'];
      } 

      return view('page.admin.member',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      if($page=='admin'){
        $detail = Admin::where('Adm_ID',$id)->first();
        $data = ['page' => $page,'detail' => $detail,'type' => 'form','action' => 'update','id' => $id];
      } else if($page=='user'){
        $detail = User::where('Use_ID',$id)->first();
        $section = Section::where('Sec_Status',1)->get();
        $data = ['page' => $page,'detail' => $detail, 'section' => $section, 'type' => 'form','action' => 'update','id' => $id];
      } else if($page=='role-admin'){
        $detail = RoleAdmin::where('Adm_ID', $id)->first();
        $data = ['page' => $page,'detail' => $detail,'type' => 'form','action' => 'update','id' => $id];
      } else if($page=='role-user'){
        $detail = RoleUser::where('Use_ID', $id)->first();
        $data = ['page' => $page,'detail' => $detail,'type' => 'form','action' => 'update','id' => $id];
      }

      return view('page.admin.member',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

    	if($page=='admin'){

          if($request->type_action=='insert'){

            $this->validate(request(), [
                'name' => 'required|string|max:255',
                'adm_username' => 'required|string|unique:admin|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'password' => 'required|string|min:6',
                'adm_email' => 'required|string|unique:admin|email|max:255',
                'tel' => 'required|digits:10|numeric',
                ]);

            $result = new Admin;
            $result->Adm_Fullname = $request->name;
            $result->Adm_Username = $request->adm_username;
            $result->Adm_Password = md5($request->password);
            $result->Adm_Email = $request->adm_email;
            $result->Adm_Tel = $request->tel;
            $result->Adm_Department = $request->department;
            $result->save();

            $result = new RoleAdmin;
            $result->Adm_ID = Admin::max('Adm_ID');
            $result->save();

            $this->LogsSystem('admin', 'insert', $request->adm_username);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';

          } else if($request->type_action=='update'){

              $this->validate(request(), [
                'name' => 'required|string|max:255',
                'tel' => 'required|digits:10|numeric',
                ]);

            	$data = [
            	'Adm_Fullname' => $request->name,
            	'Adm_Username' => $request->adm_username,
            	'Adm_Email' => $request->adm_email,
            	'Adm_Tel' => $request->tel,
              'Adm_Department' => $request->department,
            	'Adm_Permission' => $request->permission,
              'updated_at' => now(),
            	];
            	$result = Admin::where('Adm_ID',$request->id)->update($data);

              $this->LogsSystem('admin', 'update', $request->adm_username);

              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

    	} else if($page=='user'){

        	if($request->type_action=='insert'){

                $this->validate(request(), [
                'name' => 'required|string|max:255',
                'use_username' => 'required|string|unique:user|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'password' => 'required|string|min:6',
                'use_email' => 'required|string|unique:user|email|max:255',
                'tel' => 'required|digits:10|numeric',
                ]);

                $result = new User;
                $result->Use_Fullname = $request->name;
                $result->Use_Username = $request->use_username;
                $result->Use_Password = md5($request->password);
                $result->Use_Email = $request->use_email;
                $result->Use_Tel = $request->tel;
                $result->Use_Department = $request->department;
                $result->Use_Section = isset($request->section) ? $request->section : false;
                $result->save();

                $this->LogsSystem('user', 'insert', $request->use_username);

                $alert_success = 'add_success';
                $alert_not_success = 'add_not_success';

          } else if($request->type_action=='update'){

                $this->validate(request(), [
                'name' => 'required|string|max:255',
                'tel' => 'required|digits:10|numeric',
                ]);

                $data = [
                'Use_Fullname' => $request->name,
                'Use_Username' => $request->use_username,
                'Use_Email' => $request->use_email,
                'Use_Tel' => $request->tel,
                $result->Use_Department = $request->department,
                'Use_Section' => isset($request->section) ? $request->section : false,
                'Use_Permission' => $request->permission,
                ];
                $result = User::where('Use_ID',$request->id)->update($data);

                $this->LogsSystem('user', 'update', $request->use_username);

                $alert_success = 'edit_success';
                $alert_not_success = 'edit_not_success';
          }

      } else if($page=='role-admin'){

          $data = [
          'Roa_All' => (int)$request->role_all,
          'Roa_Member' => (int)$request->role_member,
          'Roa_KnowledgeAll' => (int)$request->role_knowledge_all,
          'Roa_KnowledgeCore' => (int)$request->role_knowledge_core,
          'Roa_KnowledgeAdvance' => (int)$request->role_knowledge_advance,
          'Roa_KnowledgeInnovation' => (int)$request->role_knowledge_innovation,
          'Roa_CategoryAll' => (int)$request->role_category_all,
          'Roa_CategoryCore' => (int)$request->role_category_core,
          'Roa_CategoryAdvance' => (int)$request->role_category_advance,
          'Roa_CategoryInnovation' => (int)$request->role_category_innovation,
          'Roa_Section' => (int)$request->role_section,
          'Roa_ReportsAll' => (int)$request->role_reports_all,
          'Roa_ReportsLogKnowledge' => (int)$request->role_reports_log_knowledge,
          'Roa_ReportsLogSystem' => (int)$request->role_reports_log_system,
          'Roa_ReportsMember' => (int)$request->role_reports_member,
          'Roa_ReportsVisit' => (int)$request->role_reports_visit,
          'updated_at' => now(),
          ];

          $result = RoleAdmin::where('Adm_ID',$request->id)->update($data);

          $this->LogsSystem('role', 'update', 'admin id '.$request->id);

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';

      }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	}
      	else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

  }

}

    public function delete ($page,$id) {

    	if($page=='admin'){
    	   $result = Admin::where('Adm_ID',$id)->delete();
         $this->LogsSystem('admin', 'delete', 'admin id '.$id);
    	} else if($page=='user'){
         $result = User::where('Use_ID',$id)->delete();
         $this->LogsSystem('user', 'delete', 'user id '.$id);
      }

    	if($result){
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
